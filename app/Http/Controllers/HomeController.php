<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Produk;
use App\Models\Purchase;
use App\Models\SaleStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $debt = Invoice::join('purchase_item','invoices.id','=','purchase_item.invoice_id')
                        ->select(DB::raw('SUM(purchase_item.total) as total_debt'))
                        ->get();
        $credit = Purchase::join('purchase_pay','purchase_pay.purchasing_id','=','purchasing.id')
                        ->sum('purchase_pay.total_pay');
                        // dd($credit);
        $stok = Produk::sum('qty');
        $sale = SaleStore::join('sale_store_items','sale_store.id','=','sale_store_items.sale_store_id')
                        ->sum('sale_store_items.total');
        $pembelian = Purchase::sum('total');
        $produk     = Produk::all();
        return view('admin.dashboard',compact('debt','credit','stok','sale','pembelian','produk'));
    }
}
