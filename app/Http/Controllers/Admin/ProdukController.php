<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Invoice;
use App\Models\Produk;
use App\Models\PurchaseItem;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        return view('admin.pages.product.data', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('admin.pages.product.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'kode_product' => 'required|unique:products,kode_product',
            'harga_modal' => 'required',
            'harga_pabrik' => 'required',
            'harga_distributor' => 'required',
            'category_id' => 'required',
        ]);
        $produk = new Produk();
        $produk->name = $request->name;
        $produk->description = $request->description;
        $produk->kode_product = $request->kode_product;
        $produk->harga_modal = $request->harga_modal;
        $produk->harga_pabrik = $request->harga_pabrik;
        $produk->harga_distributor = $request->harga_distributor;
        $produk->qty = 0;
        $produk->qty_retur = 0;
        $produk->category_id = $request->category_id;
        $produk->save();
        return redirect()->route('product.index')->with('success','Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        $category = Category::all();
        // dd($produk);
        return view('admin.pages.product.edit',compact('produk','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produk                     = Produk::findOrFail($id);
        $produk->name               = $request->name;
        $produk->description        = $request->description;
        $produk->kode_product       = $request->kode_product;
        $produk->harga_modal        = $request->harga_modal;
        $produk->harga_pabrik       = $request->harga_pabrik;
        $produk->harga_distributor  = $request->harga_distributor;
        $produk->qty                = $request->qty;
        $produk->qty_retur          = 0;
        $produk->category_id        = $request->category_id;
        // dd($produk);
        $produk->save();
        return redirect()->route('product.index')->with('success','Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        //
    }

    public function detailProdukItem($id)
    {
        $produk = Produk::find($id);
        return json_encode($produk);
    }

    
}
