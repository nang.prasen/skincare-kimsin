<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Produk;
use App\Models\PurchaseItem;
use App\Models\SaleStore;
use App\Models\SaleStoreItem;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SaleStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $start_date = $request->start_date ?? date('Y-m-d');
        $end_date = $request->end_date ?? date('Y-m-d');
        $penjualan = SaleStore::with('store')
                            ->join('sale_store_items','sale_store.id','=','sale_store_items.sale_store_id')
                            ->whereBetween('tanggal_sale',[$start_date,$end_date])
                            ->select('sale_store_items.harga_distributor','sale_store.*')
                            ->groupBy('kode_sale')
                            ->paginate(10);
        if ($request->key == 'filter_date') {
            return view('admin.pages.sales-store.table.data-sale-store',compact('penjualan','start_date','end_date'));
        }else {
            return view('admin.pages.sales-store.data',compact('penjualan','start_date','end_date'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tgl = date('dmY');
        $cek = SaleStore::count();
        if ($cek == 0) {
            $urut = 101;
            $kode = 'SS'.$tgl.'-'.$urut;
        } else {
            $kolom = SaleStore::all()->last();
            $urut = (int) substr($kolom->kode_sale,-3)+1; 
            $kode = 'SS'.$tgl.'-'.$urut;
        }
        $invoice = Invoice::all();
        $toko = Store::all();
        $produk = Produk::all();
        return view('admin.pages.sales-store.input',compact('kode','toko','produk','invoice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // dd($data);
        try {
            DB::beginTransaction();

            $sale = new SaleStore;
            $sale->kode_sale = $request->kode_sale;
            $sale->kode_invoice = $request->kode_inv;
            $sale->store_id = $request->store_id;
            $sale->tanggal_sale = $request->tanggal_sale;
            $sale->save();

            if ($request->qty_retur) {
                $index = 0;
                // for ($item=0; $item < count(array($data['qty_retur'])) ; $item++) { 
                    # code...
                foreach($request->qty_retur as $qty_retur){
                    $qtyy = $request->qty_retur[$index] == '' || null ? 0 : $request->qty_retur[$index];
                // foreach ($data['qty_retur'] as $item => $value) {
                    $sale_item = array(
                        'sale_store_id'          => $sale->id,
                        'product_id'             => $request->produkId[$index],
                        'qty_sale'               => $qtyy,
                        'harga_distributor'      => $request->harga_distributor[$index],
                        'total'                  => $request->harga_distributor[$index] * $qtyy,
                        'created_at'             => Carbon::now(),
                        'updated_at'             => Carbon::now(),
                    );

                    // dd($sale_item);
                    SaleStoreItem::create($sale_item);

                    // $list_item = PurchaseItem::where('id',$request->purchaseItemId[$index])->first();
                    // $list_item->qty = $list_item->qty - $qtyy;
                    // $list_item->save();

                    $product_update = Produk::where('id',$data['produkId'][$index])->first();
                    $product_update->qty = $product_update->qty - $qtyy;
                    $product_update->save();
                    $index++;
                }
            }

            $total_item_sale = SaleStoreItem::where('sale_store_id',$sale->id)->sum('qty_sale');
            SaleStore::where('id',$sale->id)->update([
                'total_qty' => $total_item_sale
            ]);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('error', $th->getMessage());
        }

        return redirect()->route('sale-store.index')->with('success','Data berhasil ditambahkan');

        // DB::beginTransaction();
        // try {
        //     //code...
        //     $sale = new SaleStore;
        //     $sale->kode_sale = $request->kode_sale;
        //     $sale->kode_invoice = $request->kode_invoice;
        //     $sale->store_id = $request->store_id;
        //     $sale->tanggal_sale = $request->tanggal_sale;
        //     $sale->save();
        //     if (!$sale->save()) {
        //         DB::rollBack();
        //         return redirect()->back()->with('error', 'gagal simpan pennjualan');
        //     }

        //     $itemsale = [];
        //     foreach ($request->item_sale as $key) {

        //         $temp['sale_store_id']      = $sale->id;
        //         $temp['product_id']         = $key['produk'];
        //         $temp['qty_sale']           = $key['qty'];
        //         $temp['harga_distributor']  = $key['harga'];
        //         $temp['total']              = $key['harga'] * $key['qty'];
        //         $temp['created_at']         = Carbon::now();
        //         $temp['updated_at']         = Carbon::now();
            
        //         array_push($itemsale, $temp);
        //     }
        //     // dd($itemsale);
        //     $item_sales = SaleStoreItem::insert($itemsale);
        //     if (!$item_sales->save()) {
        //         DB::rollBack();
        //         return redirect()->back()->with('error', 'gagal simpan item pennjualan');
        //     }
        //     $list_item = PurchaseItem::where('product_id',$itemsale['product_id'])->first();
        //     $list_item->qty = $list_item->qty - $itemsale['qty_sale'];
        //     $list_item->save();
        //     if (!$list_item->save()) {
        //         DB::rollBack();
        //         return redirect()->back()->with('error', 'gagal simpan update stok item pennjualan');
        //     }

        //     DB::commit();
        // } catch (\Throwable $th) {
        //     DB::rollBack();
        //     return redirect()->back()->with('error', $th->getMessage());
        // }
        

        // $product_update = Produk::where('id',$data['produkId'][$item])->first();
        // $product_update->qty = $product_update->qty - $data['qty_retur'][$item];
        // $product_update->save();
        // dd($item_sales);
        // $array_of_ids;
        // if ($item_sales == true) {
            // return redirect()->route('sale-store.index')->with('success','Data berhasil ditambahkan');
            
        // }
        // DB::table('purchase_item')->whereIn('product_id', $item_sales['product_id'])->update(array('qty' => $item_sales['qty']));
        // $update_pur_item = PurchaseItem::whereIn('product_id',$item_sales['product_id'])->get();
        // // dd($update_pur_item);
        // $update_pur_item->qty = $item_sales->qty_sale;
        // $update_pur_item->save();




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SaleStore  $saleStore
     * @return \Illuminate\Http\Response
     */
    public function show(SaleStore $saleStore)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SaleStore  $saleStore
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sales_store = SaleStore::with(['store','sale_item','sale_item.product'])->find($id);
        // dd($sales_store);
        // $sales_store->qty_sale = 

        
        return view('admin.pages.sales-store.edit',compact('sales_store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SaleStore  $saleStore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        
        // $itemsale_update = [];
        // foreach ($request->qty_retur as $key) {
        //     $temp['id']         = $key['produk'];
        //     $temp['qty_sale']           = $key['qty'];
        //     $temp['updated_at']         = Carbon::now();
        
        //     array_push($itemsale_update, $temp);
        // }
        // // dd($itemsale);
        // $itemsale_update = SaleStore::whereIn('id',$temp['id'])->update($itemsale_update);
        // dd($request->all());
        $sales_store = SaleStore::with(['store','sale_item','sale_item.product'])->find($id);
        // $sale_store_item = SaleStoreItem::whereIn('id',$request->)
        try {
            DB::beginTransaction();
            $sales_store->update([
                'kode_sale' => $request['kode_sale'],
                'id' => $request['id_sale_store'],
                'kode_invoice' => $request['kode_invoice'],
                'store_id' => $request['store_id'],
            ]);
            if ($request->qty_retur > 0) {
                for ($i=0; $i < count($request->qty_retur) ; $i++) { 
                    $listupdate = SaleStoreItem::find($request->iditem[$i]);
                    if ($listupdate) {
                        if ($listupdate->qty_sale == $request->qty_retur[$i]){
                        
                        } 
                        else {
                            $listupdate = SaleStoreItem::where('id',$request->iditem[$i])->first()
                            ->update([
                                'qty_sale' => $request->qty_retur[$i],
                                'update_at' => Carbon::now()
                            ]);
                            
                            $purchase_item_update = PurchaseItem::where('product_id',$request->qty_retur[$i])->first();
                            $purchase_item_update->qty = $request->qty_retur[$i] - $purchase_item_update->qty;
                            $purchase_item_update->save();
                        }
                        
                    }else{
                        DB::rollBack();
                        return back()->with('error', 'Item Edit Proses gagal ') ;
                    }
                }
            }

            DB::commit();

        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return redirect()->back()->with('error', $th->getMessage());
        }

        return redirect()->route('sale-store.index')->with('success','Behasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SaleStore  $saleStore
     * @return \Illuminate\Http\Response
     */
    public function destroy(SaleStore $saleStore)
    {
        //
    }
}
