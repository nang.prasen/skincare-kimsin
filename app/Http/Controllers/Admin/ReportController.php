<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Produk;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\SaleStore;
use App\Models\SaleStoreItem;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class ReportController extends Controller
{
    public function creditPurchase()
    {
        $piutang = Purchase::with('purchase_pay')->get();
        return view('admin.pages.report.credit-purchase',compact('piutang'));
    }

    public function debtPurchase()
    {
        $debt = Invoice::with(['store','purchase_item.product'])->get();
        // dd($debt);
        return view('admin.pages.report.debt-purchase',compact('debt'));
    }

    public function stokProduct()
    {
        // $stok = Invoice::with(['store','purchase_item.product'])->get();
        $produk = Produk::all();
        $stock = DB::table('invoices')
            ->leftJoin('purchase_item', 'invoices.id', '=', 'purchase_item.invoice_id')
            ->leftJoin('products', 'purchase_item.product_id', '=', 'products.id')
            ->leftJoin('store', 'invoices.store_id', '=', 'store.id')
            ->select('purchase_item.*', 'store.name as store_name','store.id as idstore', 'products.name as product_name',DB::raw('SUM(purchase_item.qty) as qty_total'))
            ->groupBy('invoices.store_id')
            ->get();
        $sale = SaleStoreItem::select('sale_store_items.*','products.name as nameprod',DB::raw('SUM(qty_sale) as penjualan'))
            ->join('products','products.id','=','sale_store_items.product_id')
            ->join('sale_store','sale_store.id','=','sale_store_items.sale_store_id')
            ->groupBy('sale_store.store_id')
            ->get();

            // dd($sale,$stock);
            // dd($stock);
        // $stock_detail = PurchaseItem::with(['product','invoice.store'])->sum('qty');
            // ->groupBy('purchase_item.product_id');
        // dd($stock_detail);
        return view('admin.pages.report.stock-product',compact('stock','produk','sale'));
    }

    public function detailStockProduct($id)
    {
        $store = Store::find($id);

        $detail = PurchaseItem::whereHas('invoice',function($q) use ($id){
            $q->where('invoices.store_id',$id);
        })
        ->select('*', DB::raw('SUM(qty) as total_qty,SUM(total) as total_rupiah'))
        ->groupBy('product_id')
        ->get();

        $stok_toko = SaleStore::select(DB::raw('SUM(total_qty) as stok_penjualan_toko'))->where('store_id',$id)->get();

        // dd($detail);
        // dd($stok_toko);

        return view('admin.pages.report.cetak.detail-stock-product',compact('detail','store','stok_toko'));
    }

    public function exportPdfSuratJalan($id)
    {
        // $surat_jalan = DB::table('invoices')
        //     ->leftJoin('purchase_item', 'invoices.id', '=', 'purchase_item.invoice_id')
        //     ->leftJoin('products', 'purchase_item.product_id', '=', 'products.id')
        //     ->leftJoin('store', 'invoices.store_id', '=', 'store.id')
        //     ->select('purchase_item.*', 'store.name as store_name', 'products.name as product_name')
        //     ->get();
        $surat_jalan = Invoice::with(['store','purchase_item.product'])->find($id);
        $pdf = PDF::loadView('admin.pages.report.cetak.surat-jalan',compact('surat_jalan'));
        return $pdf->download('surat-jalan.pdf');
    }
    
    public function viewPdfPo($id)
    {
        $surat_po = Purchase::with(['invoice'])->find($id);
        $item_po = DB::table('purchase_item')
                        ->leftJoin('invoices', 'purchase_item.invoice_id', '=', 'invoices.id')
                        ->leftJoin('products', 'purchase_item.product_id', '=', 'products.id')
                        ->leftJoin('purchasing', 'purchasing.id', '=', 'invoices.purchasing_id')
                        ->select('purchase_item.*','products.name as name_product',
                            DB::raw('SUM(purchase_item.qty) as qty_total'),
                            DB::raw('SUM(purchase_item.total) as total_rupiah'),
                            )
                            ->groupBy('purchase_item.product_id')
                        ->where('purchasing.id',$id)
                        ->get();
                        // dd($surat_po);
        $surat_inv = DB::table('purchasing')
        ->leftJoin('invoices', 'purchasing.id', '=', 'invoices.purchasing_id')
        ->leftJoin('store', 'invoices.store_id', '=', 'store.id')
        ->leftJoin('purchase_item', 'purchase_item.invoice_id', '=', 'invoices.id')
        ->leftJoin('products', 'purchase_item.product_id', '=', 'products.id')
        ->select('purchasing.kode_po as kodepo','purchasing.total as total_po','purchasing.dp as dp_po',
                'purchasing.sisa as sisa_po','purchasing.tanggal_po as tgl_po','invoices.kode_invoice as kodeinv','invoices.tanggal_invoice as tgl_inv','store.name as store_name',
                'purchase_item.qty as qty_item','purchase_item.harga_distributor as hrg_item',
                'purchase_item.total as total_item','invoices.id as id_inv','products.name as name_products')
        ->where('purchasing.id',$id)
        // ->groupBy('store.id')
        ->get();
        // $arr = json_decode($surat_inv);
        // dd($surat_inv);

        // $surat_jalan = Invoice::with(['store','purchase_item.product'])
        // ->whereIn('id', [$arr->id])
        // ->get();
        // dd($surat_jalan);
        // $detail_po = PurchaseItem::with('product')->where('purchase_id',$id)->get();
        return view('admin.pages.report.cetak.cetak-po',compact('surat_po','item_po','surat_inv'));
    }
    
    public function exportPo($id)
    {
        $surat_po = Purchase::with(['invoice'])->find($id);
        $item_po = DB::table('purchase_item')
                        ->leftJoin('invoices', 'purchase_item.invoice_id', '=', 'invoices.id')
                        ->leftJoin('products', 'purchase_item.product_id', '=', 'products.id')
                        ->leftJoin('purchasing', 'purchasing.id', '=', 'invoices.purchasing_id')
                        ->select('purchase_item.*','products.name as name_product',
                            DB::raw('SUM(purchase_item.qty) as qty_total'),
                            DB::raw('SUM(purchase_item.total) as total_rupiah'),
                            )
                            ->groupBy('purchase_item.product_id')
                        ->where('purchasing.id',$id)
                        ->get();
                        // dd($surat_po);
        $surat_inv = DB::table('purchasing')
        ->leftJoin('invoices', 'purchasing.id', '=', 'invoices.purchasing_id')
        ->leftJoin('store', 'invoices.store_id', '=', 'store.id')
        ->leftJoin('purchase_item', 'purchase_item.invoice_id', '=', 'invoices.id')
        ->leftJoin('products', 'purchase_item.product_id', '=', 'products.id')
        ->select('purchasing.kode_po as kodepo','purchasing.total as total_po','purchasing.dp as dp_po',
                'purchasing.sisa as sisa_po','purchasing.tanggal_po as tgl_po','invoices.kode_invoice as kodeinv','invoices.tanggal_invoice as tgl_inv','store.name as store_name',
                'purchase_item.qty as qty_item','purchase_item.harga_distributor as hrg_item',
                'purchase_item.total as total_item','invoices.id as id_inv','products.name as name_products')
        ->where('purchasing.id',$id)
        // ->groupBy('store.id')
        ->get();
        $pdf = PDF::loadView('admin.pages.report.cetak.cetak-po',compact('item_po','surat_po','surat_inv'));
        return $pdf->download('purchase-order.pdf');
    }
    
    public function cobacetak()
    {
        $pdf = PDF::loadView('admin.pages.report.cetak.cetak-test');
        return $pdf->download('purchase-order.pdf');
        # code...
    }
    
}
