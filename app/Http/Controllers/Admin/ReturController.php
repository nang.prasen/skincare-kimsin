<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Produk;
use App\Models\PurchaseItem;
use App\Models\Retur;
use App\Models\ReturItem;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retur = Retur::with('store')->get();
        // dd($retur);
        return view('admin.pages.retur.data',compact('retur'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tgl = date('dmY');
        $cek = Retur::count();
        if ($cek == 0) {
            $urut = 1001;
            $kode = 'RTO'.$tgl.'-'.$urut;
        } else {
            $kolom = Retur::all()->last();
            $urut = (int) substr($kolom->no_retur,-4)+1; 
            $kode = 'RTO'.$tgl.'-'.$urut;
        }
        $toko = Store::all();
        $produk = Produk::all();
        $invoice = Invoice::all();
        return view('admin.pages.retur.create',compact('toko','produk','kode','invoice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // $request->validate([
        //     'no_retur' => 'required',
        //     'store_id' => 'required',
        //     'product_id' => 'required',
        //     'qty_retur' => 'required',
        //     'tanggal_retur' => 'required',
        // ]);
        $retur = new Retur;
        $retur->no_retur = $request->no_retur;
        $retur->store_id = $request->store;
        $retur->tanggal_retur = $request->tanggal_retur;
        $retur->save();

        $itemretur = [];
        foreach ($request->data_retur as $key) {

            $temp['retur_id'] = $retur->id;
            $temp['product_id'] = $key['produk'];
            $temp['qty_retur'] = $key['qty'];
            $temp['created_at'] = Carbon::now();
            $temp['updated_at'] = Carbon::now();

            $produk = Produk::find($key['produk']);
            $produk['qty_retur'] = $produk['qty_retur'] + $key['qty'];
            $produk->save();



            $getinv = Invoice::where('store_id',$request->store)->first();
            $purchaseitem = PurchaseItem::where('invoice_id',$getinv->id)->where('product_id',$key['produk'])->first();
            $purchaseitem['qty'] = $purchaseitem['qty'] - $key['qty'];
            

            array_push($itemretur, $temp);
        }
        // dd($retur);
        ReturItem::insert($itemretur);
        

        // $retur = new Retur;
        // $retur->no_retur = $request->no_retur;
        // $retur->store_id = $request->store_id;
        // $retur->product_id = $request->product_id;
        // $retur->qty_retur = $request->qty_retur;
        // $retur->tanggal_retur = $request->tanggal_retur;
        // $retur->save();
        return redirect()->route('retur.index')->with('success','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $retur = Retur::findOrFail($id);
        $toko = Store::all();
        $produk = Produk::all();
        return view('admin.pages.retur.edit',compact('retur','toko','produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $retur = Retur::findOrFail($id);
        $retur->no_retur = $request->no_retur;
        $retur->store_id = $request->store_id;
        $retur->product_id = $request->product_id;
        $retur->qty_retur = $request->qty_retur;
        $retur->tanggal_retur = $request->tanggal_retur;
        $retur->save();
        return redirect()->route('retur.index')->with('success','Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $retur = Retur::with('item')->find($id);

        $retur->delete();

        return redirect()->route('retur.index')->with('success', 'Retur deleted successfully');
    }

    public function getReturInvoiceDate(Request $request)
    {
        $invoice = Invoice::where('tanggal_invoice',$request->tanggal_invoice)->get();
        return response()->json($invoice);
        // return response()->json(['retur'=>$retur,'invoice'=>$invoice]);
    }
}
