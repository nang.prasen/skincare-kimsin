<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Produk;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\PurchaseItemTemp;
use App\Models\PurchasePay;
use App\Models\SaleStoreItem;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchase = Purchase::all();
        // dd($purchase);
        return view('admin.pages.purchase.data',compact('purchase'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tgl = date('dmY');
        $cek = Purchase::count();
        if ($cek == 0) {
            $urut = 1001;
            $kode = 'POKM'.$tgl.'-'.$urut;
        } else {
            $kolom = Purchase::all()->last();
            $urut = (int) substr($kolom->kode_po,-4)+1; 
            $kode = 'POKM'.$tgl.'-'.$urut;
        }

        // $tgl = date('dmY');
        $cekinv = Invoice::count();
        if ($cekinv == 0) {
            $urut = 1001;
            $kode_inv = 'POD'.$tgl.'-'.$urut;
        } else {
            $kolom = Invoice::all()->last();
            $urut = (int) substr($kolom->kode_invoice,-4)+1; 
            $kode_inv = 'POD'.$tgl.'-'.$urut;
        }
        $refresh = PurchaseItemTemp::where('invoice_id',NULL)->delete();

        $store = Store::all();
        $produk = Produk::all();
        return view('admin.pages.purchase.create',compact('store','kode','kode_inv','produk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $purchase = new Purchase();
        $purchase->kode_po      = $request->kode_po;
        $purchase->qty          = $request->qty_po;
        $purchase->tanggal_po   = $request->tanggal_po;
        $purchase->jatuh_tempo  = $request->jatuh_tempo;
        $purchase->supplier     = $request->supplier;
        $purchase->total        = $request->total_po;
        $purchase->dp           = $request->dp_po;
        $purchase->sisa         = $request->sisa;
        $purchase->save();
        
        $index_produk = 0;
        if ($request->kode_invoice > 0) {
            for ($inv=0 ; $inv < count($request->kode_invoice) ; $inv++ ) { 
                $tgl = date('dmY');
                $cekinv = Invoice::count();
                if ($cekinv == 0) {
                    $urut = 1001;
                    $kode_inv = 'POD'.$tgl.'-'.$urut;
                } else {
                    $kolom = Invoice::all()->last();
                    $urut = (int) substr($kolom->kode_invoice,-4)+1; 
                    $kode_inv = 'POD'.$tgl.'-'.$urut;
                }
                $invoice = new Invoice();
                $invoice->kode_invoice      = $kode_inv;
                $invoice->purchasing_id     = $purchase->id;
                $invoice->store_id          = $request->get('store_id')[$inv];
                $invoice->tanggal_invoice   = $request->tanggal_invoice;
                $invoice->total_qty_inv     = $request->qty_po;
                // dd($invoice);
                $result = $invoice->save();

                for ($k=0; $k < $request->get('total_produk')[$inv] ; $k++) { 
                    $item_po = new PurchaseItem();
                    $item_po->invoice_id = $invoice->id;
                    $item_po->kode_inv = $kode_inv;
                    $item_po->product_id = $request->get('product_id')[$index_produk];
                    $item_po->kode_product = $request->get('kode_product')[$index_produk];
                    $item_po->qty = $request->get('qty_item')[$index_produk];
                    $item_po->harga_pabrik = $request->get('harga_pabrik')[$index_produk];
                    $item_po->harga_distributor = $request->get('harga_distributor')[$index_produk];
                    $item_po->total = $request->get('totalitem')[$index_produk];

                    $item_po->save();
                    $index_produk++;
                    $produk = Produk::find($request->get('product_id')[$k]);
                    $produk->qty = $produk->qty + $request->get('qty_item')[$k];
                    $produk->save();
                }
            }
        }

        $count = Invoice::where('purchasing_id',$purchase->id)->count();
        // $count = $result->count();
        // $st = [];
        $tbl_inv = Invoice::where('purchasing_id',$purchase->id)->get();
        // $st = $tbl_inv;

        $qty_item   = $request->qty_item;
        $qty              = end($qty_item);

        $itemPo = [];
        $data = [];
        // foreach ($tbl_inv as $key => $row) {
        //     for ($i=0; $i < count($request->product_id); $i++) { 
        //             # code...
        //             $itemPo [] =[
        //                 'invoice_id' => $row->id,
        //                 // 'invoice_id' => $ideal,
        //                 // 'invoice_id' => json_encode($arrayIdInv) ,
        //                 'product_id' => $request->get('product_id')[$i],
        //                 'kode_product' => $request->get('kode_product')[$i],
        //                 'qty' => $request->get('qty_item')[$i],
        //                 'harga_pabrik' => $request->get('harga_pabrik')[$i],
        //                 'harga_distributor' => $request->get('harga_distributor')[$i],
        //                 'total' => $request->get('totalitem')[$i],
        //                 'created_at' => Carbon::now(),
        //                 'updated_at' => Carbon::now(),
        //             ];
        //             $produk = Produk::find($request->get('product_id')[$i]);
        //             $produk->qty = $produk->qty + $request->get('qty_item')[$i];
        //             $produk->save();
        //         }
        //         // $list = array_push($itemPo,$data);
        //     }
        // // }
        // // dd($itemPo);
        // PurchaseItem::insert($itemPo);

            

        $update_temp = DB::table('purchase_item_temp')
              ->where('status', 'temp')
              ->update([
                'invoice_id' => $purchase->id,
                'status'     => 'inv'
            ]);
        // $update = PurchaseItemTemp::where('status','=','temp')->get();
        // foreach ($update as $i) {
        //     $update->invoice_id = $invoice->id;
        //     $update->status = 'inv';
        //     $update->save();
        // }
        $idpurchase = $purchase->id;
        
        return $idpurchase;


        // return redirect()->route('purchase.index')->with('success','Data berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = Purchase::with(['purchase_pay','invoice.store','invoice.purchase_item.product'])->find($id);
        // dd($purchase);
        // $purchase_item = PurchaseItem::where('purchasing_id',$id)->get();
        // $invoice = Invoice::where('purchasing_id',$id)->first();
        return view('admin.pages.purchase.detail',compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }

    public function add_item($id)
    {
        $purchase = Purchase::find($id);
        $tgl = date('dmY');
        $cek = Invoice::count();
        if ($cek == 0) {
            $urut = 1001;
            $kode_inv = 'POD'.$tgl.'-'.$urut;
        } else {
            $kolom = Invoice::all()->last();
            $urut = (int) substr($kolom->kode_invoice,-4)+1; 
            $kode_inv = 'POD'.$tgl.'-'.$urut;
        }
        $store = Store::all();
        $produk = Produk::all();
        return view('admin.pages.purchase.add-item',compact('purchase','kode_inv','store','produk'));
    }

    public function update_add_item(Request $request)
    {
        $purchase = Purchase::where('id',$request->purchase_id)->first();
        $purchase->qty      = $request->qty_po;
        $purchase->total    = $request->total_po;
        $purchase->dp       = $request->dp_po;
        $purchase->sisa     = $request->sisa;
        $purchase->save();

        $invoice = new Invoice();
        $invoice->kode_invoice      = $request->kode_invoice;
        $invoice->purchasing_id     = $purchase->id;
        $invoice->store_id          = $request->store_id;
        $invoice->tanggal_invoice   = $request->tanggal_invoice;
        $invoice->save();

        for ($i=0; $i < count($request->product_id) ; $i++) { 
            $po_item = new PurchaseItem();
                // $po_item->purchasing_id = $purchase->id;
            $po_item->invoice_id        = $invoice->id;
            $po_item->product_id        = $request->get('product_id')[$i];
            $po_item->kode_product      = $request->get('kode_product')[$i];
            $po_item->qty               = $request->get('qty_item')[$i];
            $po_item->harga_pabrik      = $request->get('harga_pabrik')[$i];
            $po_item->harga_distributor = $request->get('harga_distributor')[$i];
            $po_item->total             = $request->get('totalitem')[$i];
            $po_item->save();

            $produk = Produk::find($request->get('product_id')[$i]);
            $produk->qty = $produk->qty + $request->get('qty_item')[$i];
            $produk->save();
        }
        return redirect()->route('purchase.index')->with('success','Data berhasil ditambahkan');
    }

    public function productDetailPurchase(Request $request)
    {
        // $u = dd($request->all());
        
        // $invoice = Invoice::where('store_id',$request->store_id)->first();
        // dd($invoice);
            // $produk = PurchaseItem::select('purchase_item.*','products.name as nama_produk','invoices.store_id','store.name as nama_store','invoices.kode_invoice')
            //                         ->join('products','products.id','=','purchase_item.product_id')
            //                         ->join('invoices','invoices.id','=','purchase_item.invoice_id')
            //                         ->join('store','store.id','=','invoices.store_id')
            //                         // ->groupBy('purchase_item.invoice_id')
            //                         ->where('invoices.store_id',$request->store_id)
            //                         // ->groupBy('purchase_item.product_id')
            //                         ->get();
            $produk = PurchaseItem::select('purchase_item.*','products.name as nama_produk','invoices.store_id','store.name as nama_store','invoices.kode_invoice',DB::raw('SUM(purchase_item.qty) as total_qty'))
                                    ->join('products','products.id','=','purchase_item.product_id')
                                    ->join('invoices','invoices.id','=','purchase_item.invoice_id')
                                    ->join('store','store.id','=','invoices.store_id')
                                    // ->groupBy('purchase_item.invoice_id')
                                    ->where('invoices.store_id',$request->store_id)
                                    ->groupBy('purchase_item.product_id')
                                    ->get();
            $sale = SaleStoreItem::select('sale_store_items.*','products.name as nameprod',DB::raw('SUM(qty_sale) as penjualan'))
                                    ->join('products','products.id','=','sale_store_items.product_id')
                                    ->join('sale_store','sale_store.id','=','sale_store_items.sale_store_id')
                                    ->where('sale_store.store_id',$request->store_id)
                                    ->groupBy('product_id')
                                    ->get();

        $produk_array = [];
        foreach($produk as $item_produk){
            $array_temp = [
                'product_id' => $item_produk->product_id,
                'nama_produk' => $item_produk->nama_produk,
                'harga_distributor' => $item_produk->harga_distributor,
                'id' => $item_produk->id,
                'kode_invoice' => $item_produk->kode_invoice,
                'total_qty' => $item_produk->total_qty,
                'store_id' => $item_produk->store_id,
                'nama_store' => $item_produk->nama_store,
            ];
            foreach($sale as $item_sale){
                if($item_produk->product_id == $item_sale->product_id){
                    $array_temp['total_qty'] = $item_produk->total_qty - $item_sale->qty_sale;
                }
            }
            $produk_array[] = $array_temp;
        }

        $akhir_qty = [
            'produk'    => $produk_array,
            'penjualan' => $sale
        ];
        
        // return dd($akhir_qty);
        return json_encode($akhir_qty);
    }
    

    public function bayarPo($id)
    {
        $purchase = Purchase::find($id);
        return view('admin.pages.purchase.bayar-po',compact('purchase'));
    }

    public function storeBayarPo(Request $request)
    {
        // dd($request->all());
        $purchase_pay                    = new PurchasePay();
        $purchase_pay->purchasing_id     = $request->purchase_id;
        $purchase_pay->kode_pay          = $request->kode_pay;
        $purchase_pay->tanggal_pay       = $request->tanggal_pay;
        $purchase_pay->total_pay         = $request->total_pay;
        $purchase_pay->status_pay        = 'bayar';
        $purchase_pay->keterangan_pay    = $request->keterangan ?? NULL;
        $purchase_pay->save();
        return redirect()->route('purchase.index')->with('success','Data berhasil ditambahkan');
    }

    public function cetakPo()
    {
        
    }

    public function addPurchaseItemTemp(Request $request)
    {
        // dd($request->all());
        $id         = $request->product_id;
        $kode_prod  = $request->kode_product;
        $qty_item   = $request->qty;
        $hrg_pbrik  = $request->harga_pabrik;
        $hrg_dist   = $request->harga_distributor;
        $total_item = $request->total;
        
        $key_id           = end($id);
        $kode_product     = end($kode_prod);
        $qty              = end($qty_item);
        $harga_pabrik     = end($hrg_pbrik);
        $harga_distributor= end($hrg_dist);
        $total            = end($total_item);
        // dd($key_id);
        $old = PurchaseItemTemp::where('product_id',$key_id)->where('status','temp')->first();
        // dd($old);
        if ($old != null) {
            
            $arr = $request->qty;
            $keys = end($arr);
            $old->qty   = $keys + $old->qty;
            $old->total = $old->qty * $old->harga_distributor;
            $old->save();
            
        }else{
            $pur_item_temp = new PurchaseItemTemp();
            $pur_item_temp->product_id        = $key_id;
            $pur_item_temp->kode_product      = $kode_product;
            $pur_item_temp->qty               = $qty;
            $pur_item_temp->harga_pabrik      = $harga_pabrik;
            $pur_item_temp->harga_distributor = $harga_distributor;
            $pur_item_temp->total             = $total;
            $pur_item_temp->status            = 'temp';
            $pur_item_temp->save();
            // for ($i=0; $i < count($request->product_id) ; $i++) { 
            //     $pur_item_temp = new PurchaseItemTemp();
            //     $pur_item_temp->product_id        = $request->get('product_id')[$i];
            //     $pur_item_temp->kode_product      = $request->get('kode_product')[$i];
            //     $pur_item_temp->qty               = $request->get('qty')[$i];
            //     $pur_item_temp->harga_pabrik      = $request->get('harga_pabrik')[$i];
            //     $pur_item_temp->harga_distributor = $request->get('harga_distributor')[$i];
            //     $pur_item_temp->total             = $request->get('total')[$i];
            //     $pur_item_temp->status            = 'temp';
            //     $pur_item_temp->save();
            // }
        }
        
    }

    public function deletePurchaseItemTemp(Request $request)
    {
        $data = PurchaseItemTemp::where('product_id',$request->id)->where('invoice_id','=',null)->where('status','temp')->first();
        $data->delete();
    }

    public function getPurchaseItemTemp()
    {
        $purchasetemp = PurchaseItemTemp::with('produk')->where('invoice_id','=',null)->where('status','temp')->get();
        // dd($purchasetemp);
        return view('admin.pages.purchase.temp.table',compact('purchasetemp'));
    }
}
