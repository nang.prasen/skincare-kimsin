<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Store;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = Agent::all();
        return view('admin.pages.agent.data',compact('agents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tgl = date('dmY');
        $cek = Agent::count();
        if ($cek == 0) {
            $urut = 1001;
            $kode = 'AGKM'.$tgl.'-'.$urut;
        } else {
            $kolom = Agent::all()->last();
            $urut = (int) substr($kolom->kode_agent,-4)+1; 
            $kode = 'AGKM'.$tgl.'-'.$urut;
        }
        $stores = Store::all();
        return view('admin.pages.agent.create',compact('kode','stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $store = Store::where('id',$request->store_id)->first();
        if ($store) {
            $agent = new Agent();
            $agent->name = $request->name;
            $agent->kode_agent = $request->kode_agent;
            $agent->store_id = $request->store_id;
            $agent->address = $request->address;
            $agent->phone = $request->phone;
            $agent->kode_store = $store->kode_store;
            // $agent->kode_store = '1111';
            // dd($agent);
            $agent->save();
        }else{
            return redirect()->back()->with('error','Store tidak ditemukan');
        }
        return redirect()->route('agent.index')->with('success','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
        //
    }
}
