<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        return view('admin.pages.store.data',compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tgl = date('dmY');
        $cek = Store::count();
        if ($cek == 0) {
            $urut = 1001;
            $kode = 'TKM'.$tgl.'-'.$urut;
        } else {
            $kolom = Store::all()->last();
            $urut = (int) substr($kolom->kode_store,-4)+1; 
            $kode = 'TKM'.$tgl.'-'.$urut;
        }
        return view('admin.pages.store.create',compact('kode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'kode_store' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);

        

        $store = new Store;
        $store->name = $request->name;
        $store->kode_store = $request->kode_store;
        $store->address = $request->address;
        $store->phone = $request->phone;
        $store->email = $request->email;
        $store->logo = 'logo.png';
        $store->save();
        return redirect()->route('store.index')->with('success','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        $store = Store::findOrFail($store->id);
        return view('admin.pages.store.edit',compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'kode_store' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);
        $store = Store::findOrFail($id);
        $store->name = $request->name;
        $store->kode_store = $request->kode_store;
        $store->address = $request->address;
        $store->phone = $request->phone;
        $store->email = $request->email;
        $store->logo = 'logo.png';
        $store->save();
        return redirect()->route('store.index')->with('success','Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }
}
