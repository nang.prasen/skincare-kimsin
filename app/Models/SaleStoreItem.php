<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleStoreItem extends Model
{
    use HasFactory;
    protected $table = 'sale_store_items';
    protected $fillable = [
        'sale_store_id',
        'product_id',
        'qty_sale',
        'harga_distributor',
        'total',
    ];

    public function sale()
    {
        return $this->belongsTo(SaleStore::class, 'id');
    }

    public function product()
    {
        return $this->hasMany(Produk::class, 'id','product_id');
    }
}
