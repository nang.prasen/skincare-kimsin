<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturItem extends Model
{
    use HasFactory;
    protected $table = 'retur_items';
    protected $fillable = [
        'retur_id',
        'product_id',
        'qty_retur',
    ];

    public function retur()
    {
        return $this->belongsTo(Retur::class, 'id');
    }
    public function product()
    {
        return $this->belongsTo(Produk::class, 'id');
    }
}
