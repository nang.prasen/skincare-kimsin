<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;
    protected $table = 'store';
    protected $fillable = [
        'kode_store',
        'name',
        'address',
        'phone',
        'email',
        'logo',
    ];

    public function purchase_item()
    {
        return $this->hasMany(PurchaseItem::class, 'id');
    }

    public function invoice()
    {
        return $this->hasMany(Invoice::class, 'store_id');
    }

    public function retur()
    {
        return $this->hasMany(Retur::class, 'store_id');
    }

    public function sale_store()
    {
        return $this->hasMany(SaleStore::class, 'store_id','id');
    }

    
}
