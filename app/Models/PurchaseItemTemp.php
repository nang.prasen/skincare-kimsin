<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseItemTemp extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'purchase_item_temp';

    public function produk()
    {
        return $this->belongsTo(Produk::class,'product_id','id');
    }
}
