<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchasePay extends Model
{
    use HasFactory;
    protected $table = 'purchase_pay';
    protected $fillable = [
        'purchasing_id',
        'tanggal_pay',
        'total_pay',
        'kode_pay',
        'status_pay',
        'keterangan_pay',
    ];

    public function purchase()
    {
        return $this->belongsTo(Purchase::class, 'id');
    }
}
