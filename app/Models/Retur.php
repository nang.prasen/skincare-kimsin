<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Retur extends Model
{
    use HasFactory;
    protected $table = 'retur';
    protected $fillable = [
        'no_retur',
        'tanggal_retur',
        'store_id',
    ];

    public function store()
    {
        return $this->belongsTo(Store::class, 'id');
    }
    public function item()
    {
        return $this->hasMany(ReturItem::class, 'retur_id');
    }
}
