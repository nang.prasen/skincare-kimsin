<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = [
        'name',
        'description',
        'kode_product',
        'harga_modal',
        'harga_pabrik',
        'harga_distributor',
        'qty',
        'qty_retur',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    // public function purchase()
    // {
    //     return $this->hasMany(Purchase::class, 'id');
    // }

    public function purchase_item()
    {
        return $this->hasMany(PurchaseItem::class, 'id');
    }
    
    public function purchase_item_temp()
    {
        return $this->hasMany(PurchaseItemTemp::class, 'product_id','id');
    }

    public function retur()
    {
        return $this->hasMany(ReturItem::class, 'product_id');
    }

    public function sale_item()
    {
        return $this->belongsTo(SaleStoreItem::class,'product_id','id');
    }
}
