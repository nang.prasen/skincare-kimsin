<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Invoice extends Model
{
    use HasFactory;
    protected $table = 'invoices';

    public function purchase_item()
    {
        return $this->hasMany(PurchaseItem::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function purchase()
    {
        return $this->belongsTo(Purchase::class, 'id');
    }

    public static function getId()
    {
        return $getId = DB::table('invoices')->orderBy('id','desc')->take(1)->get();
    }
}
