<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;
    protected $table = 'purchasing';
    protected $fillable = [
        'kode_po',
        'product_id',
        'store_id',
        'qty',
        'tanggal_po',
        'jatuh_tempo',
        'supplier',
        'total',
        'dp',
        'sisa',
    ];

    public function invoice()
    {
        return $this->hasMany(Invoice::class, 'purchasing_id','id');
    }

    public function purchase_pay()
    {
        return $this->hasMany(PurchasePay::class, 'purchasing_id','id');
    }
}
