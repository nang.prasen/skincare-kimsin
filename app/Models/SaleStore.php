<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleStore extends Model
{
    use HasFactory;
    protected $table = 'sale_store';
    protected $fillable = [
        'kode_sale',
        'store_id',
        'tanggal_sale',
    ];

    public function store()
    {
        return $this->belongsTo(Store::class,'store_id', 'id');
    }

    public function sale_item()
    {
        return $this->hasMany(SaleStoreItem::class, 'sale_store_id');
    }
}
