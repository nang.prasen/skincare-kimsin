<?php

use App\Http\Controllers\Admin\AgentController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProdukController;
use App\Http\Controllers\Admin\PurchaseController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\ReturController;
use App\Http\Controllers\Admin\SaleStoreController;
use App\Http\Controllers\Admin\StoreController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login-kimsin');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('category',CategoryController::class);
Route::resource('product', ProdukController::class);
Route::resource('store', StoreController::class);
Route::resource('sale-store', SaleStoreController::class);
Route::resource('retur', ReturController::class);
Route::resource('agent', AgentController::class);
Route::resource('purchase', PurchaseController::class);
Route::get('purchase/add/{id}', [PurchaseController::class,'add_item'])->name('purchase.add');
Route::get('purchase/bayar/detail/{id}', [PurchaseController::class,'bayarPo'])->name('purchase.detail-bayar');
Route::patch('bayar/purchase', [PurchaseController::class,'storeBayarPo'])->name('purchase.bayar-po');
Route::post('update/purchase/item-po', [PurchaseController::class,'update_add_item'])->name('purchase.item-po.update');
Route::post('insert/purchase/item-temp', [PurchaseController::class,'addPurchaseItemTemp'])->name('purchase.item-temp');
Route::get('list/purchase/item-temp', [PurchaseController::class,'getPurchaseItemTemp'])->name('getpurchase.item-temp');
Route::delete('delete/purchase/item-temp', [PurchaseController::class,'deletePurchaseItemTemp'])->name('deletepurchase.item-temp');

Route::get('produk/detail/{id}', [ProdukController::class,'detailProdukItem'])->name('produk.detail-item');
Route::get('produk/purchase/item',[PurchaseController::class,'productDetailPurchase'])->name('product.detail-purchase');
Route::get('filter/tanggal/invoice',[ReturController::class,'getReturInvoiceDate'])->name('tanggal.invoice');

Route::get('report/credit-purchase',[ReportController::class,'creditPurchase'])->name('report.credit-purchase');
Route::get('report/debt-purchase',[ReportController::class,'debtPurchase'])->name('report.debt-purchase');
Route::get('report/stock-product',[ReportController::class,'stokProduct'])->name('report.stok-product');
Route::get('report/stock-product/detail/{id}',[ReportController::class,'detailStockProduct'])->name('report.detail-stock');
Route::get('cetak/report/surat-jalan/{id}',[ReportController::class,'exportPdfSuratJalan'])->name('cetak.surat-jalan');
Route::get('cetak/report/po/{id}',[ReportController::class,'viewPdfPo'])->name('cetak.surat-po');
Route::get('cetak/report/cetak/{id}',[ReportController::class,'exportPo'])->name('cetak.pdf.po');
