@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Apps</a></li>
            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
        </ol>
    </nav>
    {{-- <div class="page-options">
        <a href="#" class="btn btn-secondary">Settings</a>
        <a href="#" class="btn btn-primary">Upgrade</a>
    </div> --}}
</div>
<div class="main-wrapper">
    <div class="row stats-row">
        <div class="col-lg-4 col-md-12">
            <div class="card card-transparent stats-card">
                <div class="card-body">
                    <div class="stats-info">
                        @foreach ($debt as $item)
                            <h5 class="card-title">{{number_format($item->total_debt)}}</h5>
                        @endforeach
                        <p class="stats-text"><span class="badge badge-danger">Piutang (Rp)</span></p>
                    </div>
                    <div class="stats-icon change-danger">
                        <i class="material-icons">account_balance_wallet</i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="card card-transparent stats-card">
                <div class="card-body">
                    <div class="stats-info">
                        <h5 class="card-title">{{number_format($credit)}}</h5>
                        <p class="stats-text"><span class="badge badge-success">Hutang (Rp)</span></p>
                    </div>
                    <div class="stats-icon change-success">
                        <i class="material-icons">paid</i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="card card-transparent stats-card">
                <div class="card-body">
                    <div class="stats-info">
                        <h5 class="card-title">{{$stok}}</h5>
                        <p class="stats-text"><span class="badge badge-warning">Stok</span></p>
                    </div>
                    <div class="stats-icon change-success">
                        <i class="material-icons">inventory_2</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card savings-card">
                <div class="card-body">
                    <h5 class="card-title">Penjualan<span class="card-title-helper">30 Days</span></h5>
                    <div class="savings-stats">
                        <h5>{{number_format($sale)}}</h5>
                        <span>Total savings for last month</span>
                    </div>
                    {{-- <div id="sparkline-chart-1"></div> --}}
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card savings-card">
                <div class="card-body">
                    <h5 class="card-title">Pembelian<span class="card-title-helper">30 Days</span></h5>
                    <div class="savings-stats">
                        <h5>{{number_format($pembelian)}}</h5>
                        <span>Total savings for last month</span>
                    </div>
                    {{-- <div id="sparkline-chart-1"></div> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-transactions">
                <div class="card-body">
                    <h5 class="card-title">List Produk<a href="#" class="card-title-helper blockui-transactions"><i class="material-icons">refresh</i></a></h5>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Produk</th>
                                    <th scope="col">Kode</th>
                                    <th scope="col">Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($produk as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->kode_product}}</td>
                                        <td>{{$item->qty}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table> 
                    </div>     
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

