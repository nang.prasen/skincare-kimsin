<!-- Javascripts -->
<script src="{{url('kimsin-theme/assets/plugins/jquery/jquery-3.4.1.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/bootstrap/popper.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/apexcharts/dist/apexcharts.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/blockui/jquery.blockUI.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/flot/jquery.flot.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/flot/jquery.flot.time.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/flot/jquery.flot.symbol.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/js/connect.min.js')}}"></script>
<script src="{{url('kimsin-theme/assets/js/pages/dashboard.js')}}"></script>
<script src="{{url('kimsin-theme/assets/js/pages/datatables.js')}}"></script>

<script src="{{url('kimsin-theme/assets/plugins/DataTables/datatables.min.js')}}"></script>

<script src="{{url('kimsin-theme/assets/js/pages/select2.js')}}"></script>