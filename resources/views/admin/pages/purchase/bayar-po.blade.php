@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Data PO</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$purchase->kode_po}}</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <p class="page-desc">Input data dengan benar</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Input Pembayaran</h5>
                    <form action="{{route('purchase.bayar-po')}}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="exampleInputEmail1">No PO</label>
                            <input type="hidden" name="purchase_id" value="{{$purchase->id}}">
                            <input type="text" name="kode_pay" class="form-control" value="{{$purchase->kode_po}}" readonly id="kode_pay" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Tanggal PO</label>
                                    {{-- <textarea name="address" class="form-control" id="" cols="10" rows="3"></textarea> --}}
                                    <input type="date" name="tanggal_po" id="tanggal_po" value="{{$purchase->tanggal_po}}" readonly class="form-control" aria-describedby="name"
                                        placeholder="Masukan nama kategori">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Jatuh Tempo</label>
                                    <input type="date" name="jatuh_tempo" id="jatuh_tempo_po" value="{{$purchase->jatuh_tempo}}" readonly class="form-control" aria-describedby="name"
                                        placeholder="Masukan nomor hp toko">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Total : </label>
                                    <span>Rp {{number_format($purchase->total,0, ",", ".")}} </span>
                                    <input type="hidden" readonly name="total" id="total_po" value="{{$purchase->total}}" readonly class="form-control"
                                        aria-describedby="name" placeholder="Masukan Total">
                                    {{-- <label>Tambah</label> --}}
                                    <input type="hidden" readonly name="total" id="total_po_tambah" value="0" readonly class="form-control"
                                        aria-describedby="name" placeholder="Masukan Total">
                                    {{-- <label>Total</label> --}}
                                    <input type="number" readonly name="total" id="jumlah_total_po_tambah" value="{{$purchase->total}}" readonly class="form-control"
                                        aria-describedby="name" placeholder="Masukan Total">
                                    
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>DP : </label>
                                    <span>Rp {{number_format($purchase->dp,0, ",", ".")}}</span>
                                    + <span id="isi_dp_po"></span> = <span id="total_dp"></span>
                                    <input type="hidden" id="dp_po" value="{{$purchase->dp}}">
                                    <input type="number" name="total_pay" id="total_pay" class="form-control"
                                        aria-describedby="name" placeholder="Masukan Pembayaran">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Sisa : </label>
                                    <span>Rp {{number_format($purchase->sisa,0, ",", ".")}}</span>
                                    <input type="hidden" id="sisa_po" value="{{$purchase->sisa}}">
                                     <input type="number" name="sisa" id="sisa_po_tambahan" value="0" class="form-control"
                                        aria-describedby="name" placeholder="Sisa" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Tanggal Bayar</label>
                                    {{-- <textarea name="address" class="form-control" id="" cols="10" rows="3"></textarea> --}}
                                    <input type="date" name="tanggal_pay" id="tanggal_pay" class="form-control" aria-describedby="name"
                                        placeholder="Masukan nama kategori">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <input type="text" name="keterangan" class="form-control" id="" cols="3" rows="3"></input>
                                    
                                </div>
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}" />


@endsection
@push('after-scripts')
<script src="{{ url('kimsin-theme/assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="https://cdn.rawgit.com/igorescobar/jQuery-Mask-Plugin/1ef022ab/dist/jquery.mask.min.js"></script>

<script>
    $(document).ready(function() {
        // $( '#dp_po_tambahan' ).mask('#.##0', {reverse: true});
        $('.select2').select2();
    });
    $("#total_pay").keyup(function (e) { 
        // alert('test');
        var isidp = $("#total_pay").val();
        var sisa = $("#sisa_po").val();
        var dp_po_db = $("#dp_po").val();
        var sisa_pembayaran = sisa - isidp;
        $("#sisa_po_tambahan").val(sisa_pembayaran);
        // $("#isi_dp_po").text('Rp '+number_format(isidp,0, ",", "."));
        var tambahan_dp = isidp;
        var total_dp = parseInt(tambahan_dp)  + parseInt(dp_po_db) ;
        $("#isi_dp_po").text(tambahan_dp);
        $("#total_dp").text(total_dp);

    });
</script>
@endpush

