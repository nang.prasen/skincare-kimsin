<table class="tablePo table table-bordered po-list">
    <thead>
        <tr>
            <td>Produk</td>
            <td>Harga Pabrik</td>
            <td>Harga Distributor</td>
            <td>Qty</td>
            <td>Total</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($purchasetemp as $item)
            <tr>
                <td>{{$item->produk->name}}</td>
                <td>{{$item->harga_pabrik}}</td>
                <td>{{$item->harga_distributor}}</td>
                <td>{{$item->qty}}</td>
                <td>{{$item->total}}</td>
            </tr>
        @endforeach

    </tbody>

    <tfoot>
        <tr>
            <td colspan="4">Grand Total</td>
            <td>Rp <span class="grandtotal"></span></td>
            <td colspan="1"></td>
        </tr>
    </tfoot>
</table>