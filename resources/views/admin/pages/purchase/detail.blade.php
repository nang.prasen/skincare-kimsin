@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('purchase.index')}}">Daftar PO</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail {{$purchase->kode_po}}</li>
        </ol>
    </nav>
</div>
@if (session('success'))
<div class="alert alert-primary outline-alert" role="alert">
    {{ session('success') }}
    {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button> --}}
</div>
@endif
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body" style="background-color: bisque;">

                    <h5 class="card-title">Kode Po   : {{$purchase->kode_po}}</h5> 
                    <h5 class="card-title">Tanggal   : {{$purchase->tanggal_po}}</h5> 
                    <h5 class="card-title">Tempo     : {{$purchase->jatuh_tempo}}</h5> 
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body" style="background-color: bisque;">

                    <h5 class="card-title">Total     : @currency($purchase->total)</h5> 
                    <h5 class="card-title">Dp        : @currency($purchase->dp)</h5> 
                    <h5 class="card-title">Sisa      : @currency($purchase->sisa)</h5> 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Total Bayar</th>
                <th>Tanggal</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($purchase->purchase_pay as $item)    
                    <tr>
                    <th>{{$loop->iteration}}</th>
                    <td>@currency($item->total_pay)</td>
                    <td>{{$item->tanggal_pay}}</td>
                    </tr>
                @endforeach
            </tbody>
          </table>
    </div>
    <hr>
    @foreach ($purchase->invoice as $inv)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Item Purchase</h5>
                        <h5 class="card-title">Invoice : {{$inv->kode_invoice}}</h5>
                        <h5 class="card-title">Toko    : {{$inv->store->name ?? ''}}</h5>
                        <h5 class="card-title">Tanggal : {{$inv->tanggal_invoice}}</h5>
                        <br>
                        <a href="{{route('cetak.surat-jalan',$inv->id)}}" target="_blank"><i class="material-icons">file_download</i>Cetak Surat Jalan</a>
                        <table class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Produk</th>
                                    <th>Harga Distributor</th>
                                    <th>Jumlah</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($inv->purchase_item as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$item->product->name ?? ''}}</td>
                                        <td>@currency($item->harga_distributor)</td>
                                        <td>{{$item->qty}}</td>
                                        <td>@currency($item->total)</td>
                                    </tr>    
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    
    
</div>




@endsection
@push('after-scripts')
    
@endpush

