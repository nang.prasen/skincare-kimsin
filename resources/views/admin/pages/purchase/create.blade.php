@extends('admin.layouts.layout-dashboard')
@section('content')
    <!-- Page-header end -->

    <div class="page-info">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Data PO</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form Input</li>
            </ol>
        </nav>
    </div>
    <div class="main-wrapper" id="inputFormPo">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <p class="page-desc">Input PO data dengan benar</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" id="hitung-po">
                        <h5 class="card-title">Input Purchase</h5>
                        {{-- <form action="{{ route('purchase.store') }}" method="POST" enctype="multipart/form-data"> --}}
                            @csrf
                            <div class="form-group">
                                <label>Kode PO</label>
                                <input type="text" name="kode_po" id="kodepo_po" value="{{ $kode }}" readonly
                                    class="form-control" aria-describedby="name" placeholder="Masukan kode produk">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Tanggal PO</label>
                                        {{-- <textarea name="address" class="form-control" id="" cols="10" rows="3"></textarea> --}}
                                        <input type="date" name="tanggal_po" id="tanggal_po" class="form-control" aria-describedby="name"
                                            placeholder="Masukan nama kategori">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Jatuh Tempo</label>
                                        <input type="date" name="jatuh_tempo" id="jatuh_tempo_po" class="form-control" aria-describedby="name"
                                            placeholder="Masukan nomor hp toko">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label>Supplier</label>
                                <input type="text" name="supplier" readonly value="Kimsin Beauty" id="supplier_po" class="form-control" aria-describedby="name"
                                    placeholder="Kimsin Beauty">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <input type="number" name="qty" id="qty_po" class="form-control total_qty_po" value="0" readonly aria-describedby="name"
                                            placeholder="Masukan stok">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Total</label>
                                        <input type="number" readonly name="total" id="total_po" value="0" readonly class="form-control"
                                            aria-describedby="name" placeholder="Masukan Total">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>DP</label>
                                        <input type="number" name="dp" id="dp_po" class="form-control"
                                            aria-describedby="name" placeholder="Masukan DP">
                                    </div>
                                    di isi 0 jika tidak ada DP
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Sisa</label>
                                        <input type="number" name="sisa" id="sisa_po" class="form-control"
                                            aria-describedby="name" placeholder="Sisa" readonly>
                                    </div>
                                </div>
                            </div>
                            {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
                        {{-- </form> --}}
                        <br>
                        <div id="po-list-table" style="background-color:#fff4e6">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col detail-item">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Detail Item Purchase</h5>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Kode Invoice</label>
                                    <input type="text" name="kode_po" id="kodeinv" value="{{ $kode_inv }}" readonly class="kodeinv form-control"
                                        aria-describedby="name" placeholder="Masukan kode produk">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Tanggal Invoice</label>
                                    <input type="date" name="tanggal_po" id="tanggalinv" class="form-control"
                                        aria-describedby="name" placeholder="Masukan nama kategori">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" name="total_qty_inv" id="total_qty_inv" class="form-control total_qty_inv" readonly>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Toko</label>
                                    <select name="store_id" id="store_id_po" class="select2 form-control store_id_po" tabindex="-1"
                                        style="display: none; width: 100%">
                                        <option value="">--Pilih Toko-- </option>
                                        @foreach ($store as $item)
                                            <option name="store_id" value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Item Produk</h5>
                        <form>
                            <div class="row detail-item">
                                <div class="col">
                                    <select name="product_id" class="produk-action select2 form-control" tabindex="-1"
                                        style="display: none; width: 100%">
                                        <option value="">--Pilih Produk-- </option>
                                        @foreach ($produk as $item)
                                            <option name="product_id" value="{{ $item->id }}">{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="text" name="kode_produk"
                                        class="form-control form-control-sm kode_produk"  readonly/>
                                </div>
                                <div class="col">
                                    <input type="text" name="harga_pabrik"
                                        class="form-control form-control-sm hrgpabrik" />
                                </div>
                                <div class="col">
                                    <input type="text" name="harga_distributor"
                                        class="form-control form-control-sm hrgdis" />
                                </div>
                                <div class="col">
                                    <input type="text" name="qty"
                                        class="form-control form-control-sm qtyitem" placeholder="masukkan jumlah barang" />
                                </div>
                                <input type="hidden" name="total_produk[]"
                                class="form-control form-control-sm total_produk" placeholder="masukkan jumlah barang" value="0"/>
                                <div class="col">
                                    <button type="button" class="btn btn-warning btn-sm addTable">
                                        Masukan Item</button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <hr>
                        <table class="myTable table table-bordered order-list">
                            <thead>
                                <tr>
                                    <td>Produk</td>
                                    <td>Harga Pabrik</td>
                                    <td>Harga Distributor</td>
                                    <td>Qty</td>
                                    <td>Total</td>
                                    <td>#</td>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="4">Grand Total</td>
                                    <td>Rp <span class="grandtotal"></span></td>
                                    <td colspan="2"></td>
                                </tr>
                            </tfoot>
                        </table>


                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="formNew">

        </div>
        <a href="javascript:void(0)" class="btn btn-primary" onclick="addRowFormToko()">Tambah Toko</a>
        <hr>
        <button class="btn btn-warning" type="button" id="simpansemua">Simpan</button>
        {{-- <button class="btn btn-primary" type="button" style="display: none;" id="cetak_po">Cetak PO</button> --}}
        <div class="cetak_po"></div>
        {{-- <a href="" target="_blank" style="display: none;" id="cetak_po" class="btn btn-success">cetak</a> --}}
        {{-- <button class="btn btn-secondary" type="button" style="display: none;" id="cetak_surat_jalan">Cetak Surat Jalan</button> --}}
    </div>
    <div id="afterInputPo">

    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
@push('after-scripts')
    <script src="{{ url('kimsin-theme/assets/plugins/select2/js/select2.full.min.js') }}"></script>
    {{-- <script src="{{ url('js/jquery.js') }}"></script> --}}
    {{-- <script src="{{ url('js/jquery.validate.js') }}"></script> --}}
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {
        //     $("#inputFormPo").validate({
		// 	rules: {
		// 		dp_po: "required",
		// 		tanggal_po: "required",
		// 	},
		// 	messages: {
		// 		dp_po: "Masukan DP dahulu",
		// 		tanggal_po: "Tanggal PO belum terisi",
		// 	}
		// });

            $('#store_ids').select2();
            // var counter = 0;

            // untuk atas
            $(document).on('change', '.produk-action', function () {
            // $(".produk-action").change(function () {
                // alert('clicked');
                var produk = $(this).val();
                var parent = $(this).parents('.detail-item')
                console.log(produk);
                console.log($(this).parents('.detail-item'));
                $.ajax({
                    url: "{{route('produk.detail-item', ':id')}}".replace(':id', produk),
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        parent.find('.hrgpabrik').val(data.harga_pabrik);
                        parent.find('.hrgdis').val(data.harga_distributor);
                        parent.find('.kode_produk').val(data.kode_product);
                    }
                });

            });

            $(document).on("click", ".addTable", function() {
                console.log('item masuk klik');
                var parent = $(this).parents('.detail-item')

                var idprod = parent.find(".produk-action").val();
                var kodeprod = parent.find(".kode_produk").val();
                var produkId = parent.find(".produk-action option:selected").text();

                var totalproduk = parent.find('.total_produk').val() 
                var total_produk = parseInt(totalproduk) + 1;
                parent.find('.total_produk').val(total_produk);

                var hargapabrik = parent.find(".hrgpabrik").val();
                var hargadis = parent.find(".hrgdis").val();
                var qty = parent.find(".qtyitem").val();
                var totalitem = hargadis * qty;

                var tr = '<tr>  <td>' + produkId + ' <input type="hidden" class="idprod_item"  value='+idprod+'> <input type="hidden" class="kodeprod_item"  value='+kodeprod+'> </td>     <td>' + hargapabrik + ' <input type="hidden" class="hargapabrik_item"  value='+hargapabrik+'> </td>   <td>' +hargadis + ' <input type="hidden" class="hargadis_item"  value='+hargadis+'> </td>   <td class="qty_untuk_po">' + qty + '<input type="hidden" class="qty_item"  value='+qty+'> </td>   <td class="totalItem">' + totalitem +' <input type="hidden" class="totalitem_item"  value='+totalitem+'> </td>   <td><input type="button" data-id="'+idprod+'" class="btn btn-danger btn-sm" id="ibtnDelTable"  value="-"></td></tr>';
                
               
                // var tr_po = '<tr>  <td>' + produkId + ' <input type="hidden" class="idprod_item"  value='+idprod+'> <input type="hidden" class="kodeprod_item"  value='+kodeprod+'> </td>     <td>' + hargapabrik + ' <input type="hidden" class="hargapabrik_item"  value='+hargapabrik+'> </td>   <td>' +hargadis + ' <input type="hidden" class="hargadis_item"  value='+hargadis+'> </td>   <td class="qty_untuk_po">' + qty + '<input type="hidden" class="qty_item"  value='+qty+'> </td>   <td class="totalItem">' + totalitem +' <input type="hidden" class="totalitem_item"  value='+totalitem+'> </td>   <td><input type="button" class="btn btn-danger btn-sm" id="ibtnDelTable"  value="-"></td></tr>';
                parent.find(".myTable tbody").append(tr);
                // $('.tablePo tbody').parents('#po-list-table').find(".tablePo tbody").append(tr_po);

                var subTotal = 0;
                parent.find('tbody tr').each(function() {
                    var totalitem = $(this).find('.totalItem').text();
                    subTotal += +totalitem;
                });

                $('.tablePo tbody').parents('#po-list-table').find('tbody tr').each(function() {
                    var totalitem = $(this).find('.totalItem').text();
                    subTotal += +totalitem;
                });

                parent.find('.grandtotal').text(subTotal.toFixed(2))
                // $('.tablePo tbody').parents('#po-list-table').find('.grandtotal').text(subTotal.toFixed(2))



                addPurchaseItemTemp();
                sumtotalitem();
                totalQtyPo();
                totalQtyInv();
            });

            $("table.order-list").on("click", "#ibtnDelTable", function(event) {
                $(this).closest("tr").remove();
                var id = $(this).attr('data-id');

                deletePurchaseItemTemp(id);
                sumtotalitem();
                totalQtyPo();
            });

            $("#simpansemua").click(function () {
                // po
                var kodepo      = $("#kodepo_po").val();
                var store_id    = $("#store_id_po").val();
                var tanggal_po  = $("#tanggal_po").val();
                var jatuh_tempo = $("#jatuh_tempo_po").val();
                var supplier    = $("#supplier_po").val();
                var qty_po      = $("#qty_po").val();
                var total_po    = $("#total_po").val();
                var dp_po       = $("#dp_po").val();
                var sisa        = $("#sisa_po").val();
                console.log(kodepo, 'store=',store_id, 'tgl-po=',tanggal_po, 'jthtemp=',jatuh_tempo, 'sup=',supplier, 'qtypo=',qty_po, 'totpo=',total_po, 'dppo=',dp_po, 'sisa=',sisa);
                // console.log('----');
                // item
                // var kode_invoice    = $("#kodeinv").val();
                var tanggal_invoice = $("#tanggalinv").val();

                let kodeprod_item = [];
                let idprod_item = [];
                let hargapabrik_item = [];
                let hargadis_item = [];
                let qty_item = [];
                let totalitem_item = [];
                let kodeinv = [];
                let store_id_po = [];
                let total_produk = [];

                $('.main-wrapper .kodeprod_item').each(function () {
                    kodeprod_item.push($(this).val());
                });
                $('.main-wrapper .idprod_item').each(function () {
                    idprod_item.push($(this).val());
                });
                $('.main-wrapper .hargapabrik_item').each(function () {
                    hargapabrik_item.push($(this).val());
                });
                $('.main-wrapper .hargadis_item').each(function () {
                    hargadis_item.push($(this).val());
                });
                $('.main-wrapper .qty_item').each(function () {
                    qty_item.push($(this).val());
                });
                $('.main-wrapper .totalitem_item').each(function () {
                    totalitem_item.push($(this).val());
                });
                $('.main-wrapper .kodeinv').each(function () {
                    kodeinv.push($(this).val());
                });
                $('.main-wrapper .store_id_po').each(function () {
                    store_id_po.push($(this).val());
                });
                $('.main-wrapper .total_produk').each(function () {
                    total_produk.push($(this).val());
                });

                console.log('kodeprod=',kodeprod_item);
                console.log('idproduks=',idprod_item);
                console.log('hrgpabrik=',hargapabrik_item);
                console.log('hrgdistrib=',hargadis_item);
                console.log('qty=',qty_item);
                console.log('total=',totalitem_item);
                console.log('store=',store_id_po);
                console.log('store=',kodeinv);

                // console.log(kode_invoice, 'tglinv=',tanggal_invoice, 'produkid=',idprod, 'hrgpbrik=',hargapabrik, 'hrgdis',hargadis, 'qtyitem=',qty_item, 'totitem=',totalitem);
                $.ajax({
                    url: "{{ route('purchase.store') }}",
                    method: 'POST',
                    data: {
                        _token: CSRF_TOKEN,
                        kode_po: kodepo,
                        store_id: store_id_po,
                        tanggal_po: tanggal_po,
                        jatuh_tempo: jatuh_tempo,
                        supplier: supplier,
                        qty_po: qty_po,
                        total_po: total_po,
                        dp_po: dp_po,
                        sisa: sisa,
                        kode_invoice: kodeinv,
                        tanggal_invoice: tanggal_invoice,
                        product_id: idprod_item,
                        kode_product: kodeprod_item,
                        harga_pabrik: hargapabrik_item,
                        harga_distributor: hargadis_item,
                        qty_item: qty_item,
                        totalitem: totalitem_item,
                        total_produk:total_produk
                    },
                    success: function(data) {
                        console.log(data);
                        console.log('sukses simpan semua');
                        var url = "{{ url('cetak/report/po') }}/" + data;
                        $("#simpansemua").prop('disabled',true);
                        // $(".cetak_po").html(htmlString);
                        $(".cetak_po").append('<a href="'+url+'" target="_blank" id="cetak_po" class="btn btn-success">cetak</a>');
                        // $("#cetak_po").show();
                        // $("#cetak_surat_jalan").show();
                        // window.location.reload("{{ route('purchase.index') }}") ;
                    },
                    error: function(xhr, status, error) {
                        alert(xhr.responseJSON.message);
                        // alert(xhr.responseText);
                    }
                });
            });



        });

        jmlToko=1;
        kodeinv=1;
        function addRowFormToko()
        {
            row = `
            <div class="col detail-item">
                <div class="bg-info px-2 text-light text-right"><span class="cursor" onclick="deleteRowItem(`+jmlToko+`)"><i class="fa fa-trash"></i> Hapus</span></div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Detail Item Purchase</h5>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Kode Invoice</label>
                                    <input type="text" name="kode_po" id="kodeinv" value="{{$kode_inv}}" readonly class="kodeinv form-control"
                                        aria-describedby="name" placeholder="Masukan kode produk">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Tanggal Invoice</label>
                                    <input type="date" name="tanggal_po" id="tanggalinv" class="form-control"
                                        aria-describedby="name" placeholder="Masukan nama kategori">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" value="0" name="total_qty_inv" id="total_qty_inv_`+jmlToko+`" class="form-control total_qty_inv" readonly>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Toko</label>
                                    <select name="store_id" id="store_id_po-`+jmlToko+`" class="select2 form-control store_id_po" tabindex="-1"
                                        style="display: none; width: 100%">
                                        <option value="">--Pilih Toko-- </option>
                                        @foreach ($store as $item)
                                            <option name="store_id" value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Item Produk</h5>
                        <form class="form-produk">
                            <div class="row">
                                <div class="col">
                                    <select name="product_id"  class="produk-action select2 form-control" tabindex="-1" style="display: none; width: 100%">
                                        <option value="">--Pilih Produk-- </option>
                                        @foreach ($produk as $item)
                                            <option name="product_id" value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="text" name="kode_produk"
                                        class="form-control form-control-sm kode_produk"  readonly/>
                                </div>
                                <div class="col">
                                    <input type="text" name="harga_pabrik"
                                        class="form-control form-control-sm hrgpabrik" />
                                </div>
                                <div class="col">
                                    <input type="text" name="harga_distributor"
                                        class="form-control form-control-sm hrgdis" />
                                </div>
                                <div class="col">
                                    <input type="text" name="qty"
                                        class="form-control form-control-sm qtyitem" placeholder="masukkan jumlah barang" />
                                </div>
                                <input type="hidden" name="total_produk[]"
                                        class="form-control form-control-sm total_produk" placeholder="masukkan jumlah barang" value="0"/>
                                <div class="col">
                                    <button type="button" class="btn btn-warning btn-sm addTable">
                                        Masukan Item</button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <hr>
                        <table class="myTable table table-bordered order-list">
                            <thead>
                                <tr>
                                    <td>Produk</td>
                                    <td>Harga Pabrik</td>
                                    <td>Harga Distributor</td>
                                    <td>Qty</td>
                                    <td>Total</td>
                                    <td>#</td>
                                    <td>#</td>
                                </tr>
                            </thead>
                            {{-- <tbody>
                            <tr class="tr_body_clone">
                                <td>
                                    <select name="store_id" id="store_ids" class="form-control" tabindex="-1" style="display: none; width: 100%">
                                        <option value="">--Pilih Toko-- </option>
                                        @foreach ($store as $item)
                                            <option  name="store_id" value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" name="harga_pabrik" id="harga_pabrik" class="form-control form-control-sm" />
                                </td>
                                <td>
                                    <input type="text" name="harga_distributor" id="harga_distributor" class="form-control form-control-sm" />
                                </td>
                                <td>
                                    <input type="text" name="qty" id="qty" class="form-control form-control-sm" />
                                </td>
                                <td>
                                    <input type="text" name="total" id="total_item" class="form-control form-control-sm" />
                                </td>
                                <td><input type="button" class="btn btn-danger btn-sm" id="ibtnDel"  value="-"></td>
                                <td colspan="5" style="text-align: left;">
                                    <input type="button" class="btn btn-warning btn-sm addRow" id="addrow" value="+" />
                                </td>
                            </tr>
                        </tbody> --}}
                            <tbody>

                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="4">Grand Total</td>
                                    <td>Rp <span class="grandtotal"></span></td>
                                    <td colspan="2"></td>
                                </tr>
                            </tfoot>
                        </table>


                    </div>
                </div>
            </div>
            `;
            $("#formNew").append(row);
            $('.select2').select2({
                theme: 'bootstrap4'
            })
            // $("#produk").select2();
            // $('#store_ids').select2();
            jmlToko++;
            // produkChange(jmlToko);

        }

        function deleteRowItem(rowid){
            $('.detail-item-'+rowid).remove();
        }

        $(document).ready(function () {

            $(document).on("change",".coba-"+jmlToko, function () {
                alert('item masuk change');
                // var produkid = $(this).val();
                // console.log(produkid);
                // $.ajax({
                //     url: "{{route('produk.detail-item', ':id')}}".replace(':id', produkid),
                //     type: "GET",
                //     dataType: "json",
                //     success: function (data) {
                //         // console.log(data);
                //         $('#hrgpabrik-'+jmlToko).val(data.harga_pabrik);
                //         $('#hrgdis-'+jmlToko).val(data.harga_distributor);
                //         $('#kode_produk-'+jmlToko).val(data.kode_product);
                //     }
                // });
            });
        });

        function addPurchaseItemTemp(){
            let kodeprod_item = [];
            let idprod_itemy = [];
            let hargapabrik_item = [];
            let hargadis_item = [];
            let qty_item = [];
            let totalitem_item = [];
            let kode_inv = [];

            let store_id_po = [];

            $('.main-wrapper .kodeprod_item').each(function () {
                kodeprod_item.push($(this).val());
            });
            $('.main-wrapper .idprod_item').each(function () {
                idprod_itemy.push($(this).val());
            });
            $('.main-wrapper .hargapabrik_item').each(function () {
                hargapabrik_item.push($(this).val());
            });
            $('.main-wrapper .hargadis_item').each(function () {
                hargadis_item.push($(this).val());
            });
            $('.main-wrapper .qty_item').each(function () {
                qty_item.push($(this).val());
            });
            $('.main-wrapper .totalitem_item').each(function () {
                totalitem_item.push($(this).val());
            });
            $('.main-wrapper .store_id_po').each(function () {
                    store_id_po.push($(this).val());
                });


            $.ajax({
                type: "POST",
                url: "{{route('purchase.item-temp')}}",
                data: {
                    _token: CSRF_TOKEN,
                    product_id: idprod_itemy,
                    kode_product: kodeprod_item,
                    harga_pabrik: hargapabrik_item,
                    harga_distributor: hargadis_item,
                    qty: qty_item,
                    total: totalitem_item,
                },
                success: function (data) {
                    // console.log(data);
                    var url = "{{ route('getpurchase.item-temp') }}";
                    $("#po-list-table").load(url);
                    // $("#po-list-table").html(data);
                }
            });
        }

        function deletePurchaseItemTemp(id) {
            $.ajax({
                url: "{{ route('deletepurchase.item-temp') }}",
                method : "post",
                data:{
                    _token: CSRF_TOKEN,
                    _method: 'delete',
                    id:id
                },
                success: function (response) {
                    var url = "{{ route('getpurchase.item-temp') }}";
                    $("#po-list-table").load(url);   
                }
            });
        }


        function produkChange(rowid) {
            // alert("ok");
            // console.log(rowid);
            $( "#produkAdd-"+rowid ).click(function() {
                alert("boundOnPageLoaded Button Clicked")
            });
            // $("#produkAdd").off('change').on('change', function () {
            //     var produk = $(this).val();
            //     console.log(produk);
            //     $.ajax({
            //         url: "{{route('produk.detail-item', ':id')}}".replace(':id', produk),
            //         type: "GET",
            //         dataType: "json",
            //         success: function (data) {
            //             // console.log(data);
            //             $('#hrgpabrik-'+rowid).val(data.harga_pabrik);
            //             $('#hrgdis-'+rowid).val(data.harga_distributor);
            //             $('#kode_produk-'+rowid).val(data.kode_product);
            //         }
            //     });
            // });
         }


        function sumtotalitem() {
            var total = 0;
            $(".main-wrapper .myTable .grandtotal").each(function() {
                console.log($(this));
                var totalitem = $(this).text();
                total += +totalitem;
            });
            // console.log(total);
            $("#total_po").val(total);
        }

        function totalQtyPo() {
            var totalQty = 0;
            $(".myTable tbody tr .qty_untuk_po").each(function() {
                var totalQtyItem = $(this).text();
                totalQty += +totalQtyItem;
                // console.log($(this));
            });
            $("#qty_po").val(totalQty);

        }

        function totalQtyInv() { 
            var totalQtyInv = 0;
            $(".myTable tbody tr .qty_untuk_po").each(function() {
                var totalInv = $(this).text();
                totalQtyInv += +totalInv;
                // console.log($(this));
            });
            $("#total_qty_inv").val(totalQtyInv);
            
        }



        // function calculateRow(row) {
        //     var price = +row.find('#harga_distributor').val();
        // }

        // function calculateGrandTotal() {
        //     var grandTotal = 0;

        //     $("table.order-list").find('input[id^="total_item"]').each(function () {
        //         // grandTotal += +$(this).val();
        //         var total = $(this).val()
        //         grandTotal += +total;
        //         // console.log('total',total[i]);
        //     });
        //     console.log('=',grandTotal);
        //     $("#grandtotal").text(grandTotal.toFixed(2));
        // }

        $("#hitung-po").keyup(function(e) {
            var total = $("#total_po").val();
            var dp = $("#dp_po").val();
            var sisa = parseInt(total) - parseInt(dp);
            $("#sisa_po").attr("value", sisa);
        });
    </script>
@endpush
