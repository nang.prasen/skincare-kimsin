@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Extended</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Tables</li>
        </ol>
    </nav>
</div>
@if (session('success'))
<div class="alert alert-primary outline-alert" role="alert">
    {{ session('success') }}
    {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button> --}}
</div>
@endif
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <a href="{{route('purchase.create')}}" class="btn btn-primary btn-sm"  style="float: right;">buat PO</a>
                {{-- <p class="page-desc">DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, built upon the foundations of progressive enhancement, that adds many advanced features to any HTML table.</p> --}}
            </div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Purchase</h5>
                    <br>
                    <table id="zero-conf" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode PO</th>
                                <th>Tanggal</th>
                                <th>Jatuh Tempo</th>
                                <th>Total</th>
                                <th>DP</th>
                                <th>Sisa</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($purchase as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->kode_po}}</td>
                                    <td>{{$item->tanggal_po}}</td>
                                    <td>{{$item->jatuh_tempo}}</td>
                                    <td>{{$item->total}}</td>
                                    <td>{{$item->dp}}</td>
                                    <td>{{$item->sisa}}</td>
                                    <td>
                                        <a href="{{route('purchase.add',$item->id)}}" class="btn btn-secondary btn-xs">Tambah Item</a>
                                        <a href="{{route('purchase.edit',$item->id)}}" class="btn btn-secondary btn-xs">Edit</a>
                                        <a href="{{route('purchase.show',$item->id)}}" class="btn btn-warning btn-xs">Detail</a>
                                        <a href="{{route('purchase.detail-bayar',$item->id)}}" class="btn btn-warning btn-xs">Bayar</a>
                                        <a href="{{route('cetak.surat-po',$item->id)}}" target="_blank" class="btn btn-success btn-xs">cetak</a>
                                        {{-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Hapus</button> --}}
                                        <form action="{{ route('purchase.destroy',$item->id) }}" method="POST" style="display: inline-block;">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-xs" value="Delete"
                                                onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data {{ $item->name }} ?')">
                                                 Hapus
                                            </button>
                                        </form> 
                                    </td>
                                </tr>    
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>




@endsection
@push('after-scripts')
    
@endpush

