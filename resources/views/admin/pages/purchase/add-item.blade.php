@extends('admin.layouts.layout-dashboard')
@section('content')
    <!-- Page-header end -->

    <div class="page-info">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Data PO</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form Input</li>
            </ol>
        </nav>
    </div>
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <p class="page-desc">Input PO data dengan benar</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" id="hitung-po">
                        <h5 class="card-title">Input Purchase</h5>
                        {{-- <form action="{{ route('purchase.store') }}" method="POST" enctype="multipart/form-data"> --}}
                            @csrf
                            <div class="form-group">
                                <label>Kode PO</label>
                                <input type="text" name="kode_po" id="kodepo_po" value="{{ $purchase->kode_po }}" readonly
                                    class="form-control" aria-describedby="name" placeholder="Masukan kode produk">
                                    <input type="hidden" name="purchase_id" id="purchase_id" value="{{$purchase->id}}">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Tanggal PO</label>
                                        {{-- <textarea name="address" class="form-control" id="" cols="10" rows="3"></textarea> --}}
                                        <input type="date" name="tanggal_po" id="tanggal_po" value="{{$purchase->tanggal_po}}" readonly class="form-control" aria-describedby="name"
                                            placeholder="Masukan nama kategori">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Jatuh Tempo</label>
                                        <input type="date" name="jatuh_tempo" id="jatuh_tempo_po" value="{{$purchase->jatuh_tempo}}" readonly class="form-control" aria-describedby="name"
                                            placeholder="Masukan nomor hp toko">
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label>Supplier</label>
                                <input type="text" name="supplier" readonly value="Kimsin Beauty" id="supplier_po" class="form-control" aria-describedby="name"
                                    placeholder="Kimsin Beauty">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Quantity : </label>
                                        <span>{{$purchase->qty}}</span>
                                        <input type="hidden" name="qty" id="qty_po" class="form-control total_qty_po" value="{{$purchase->qty}}" readonly aria-describedby="name"
                                            placeholder="Masukan stok">
                                        {{-- <label>Quantity Tambah</label> --}}
                                        <input type="hidden" name="qty" id="qty_po_tambah" class="form-control total_qty_po_tambah" value="0" readonly aria-describedby="name"
                                            placeholder="Masukan stok">
                                        {{-- <label>Total</label> --}}
                                            <input type="number" readonly name="total" id="total_qty_tambah" value="0" readonly class="form-control"
                                                aria-describedby="name" placeholder="Masukan Total">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Total : </label>
                                        <span>@currency($purchase->total)</span>
                                        <input type="hidden" readonly name="total" id="total_po" value="{{$purchase->total}}" readonly class="form-control"
                                            aria-describedby="name" placeholder="Masukan Total">
                                        {{-- <label>Tambah</label> --}}
                                        <input type="hidden" readonly name="total" id="total_po_tambah" value="0" readonly class="form-control"
                                            aria-describedby="name" placeholder="Masukan Total">
                                        {{-- <label>Total</label> --}}
                                        <input type="number" readonly name="total" id="jumlah_total_po_tambah" value="0" readonly class="form-control"
                                            aria-describedby="name" placeholder="Masukan Total">
                                        
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>DP : </label>
                                        <span>@currency($purchase->dp)</span>
                                        <input type="hidden" id="dp_po" value="{{$purchase->dp}}">
                                        <input type="number" name="dp" id="dp_po_tambahan" value="0" class="form-control"
                                            aria-describedby="name" placeholder="Masukan DP">
                                    </div>
                                    di isi 0 jika tidak ada DP
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Sisa : </label>
                                        <span>@currency($purchase->sisa)</span>
                                        <input type="hidden" id="sisa_po" value="{{$purchase->sisa}}">
                                         <input type="number" name="sisa" id="sisa_po_tambahan" value="0" class="form-control"
                                            aria-describedby="name" placeholder="Sisa" readonly>
                                    </div>
                                </div>
                            </div>
                            {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col detail-item">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Detail Item Purchase</h5>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Kode Invoice</label>
                                    <input type="text" name="kode_po" id="kodeinv" value="{{ $kode_inv }}" readonly class="form-control"
                                        aria-describedby="name" placeholder="Masukan kode produk">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Tanggal Invoice</label>
                                    <input type="date" name="tanggal_po" id="tanggalinv" class="form-control"
                                        aria-describedby="name" placeholder="Masukan nama kategori">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Toko</label>
                                    <select name="store_id" id="store_id_po" class="js-states form-control" tabindex="-1"
                                        style="display: none; width: 100%">
                                        <option value="">--Pilih Toko-- </option>
                                        @foreach ($store as $item)
                                            <option name="store_id" value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col">
                                    <select name="product_id" id="produk" class="form-control" tabindex="-1"
                                        style="display: none; width: 100%">
                                        <option value="">--Pilih Produk-- </option>
                                        @foreach ($produk as $item)
                                            <option name="product_id" value="{{ $item->id }}">{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="text" name="kode_produk" id="kode_produk"
                                        class="form-control form-control-sm"  readonly/>
                                </div>
                                <div class="col">
                                    <input type="text" name="harga_pabrik" id="hrgpabrik"
                                        class="form-control form-control-sm" />
                                </div>
                                <div class="col">
                                    <input type="text" name="harga_distributor" id="hrgdis"
                                        class="form-control form-control-sm" />
                                </div>
                                <div class="col">
                                    <input type="text" name="qty" id="qtyitem"
                                        class="form-control form-control-sm" placeholder="masukkan jumlah barang" />
                                </div>
                                <div class="col">
                                    <button type="button" class="btn btn-warning btn-sm addTable" id="addTable">
                                        Masukan Item</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <table id="myTable" class="table table-bordered order-list">
                            <thead>
                                <tr>
                                    <td>Produk</td>
                                    <td>Harga Pabrik</td>
                                    <td>Harga Distributor</td>
                                    <td>Qty</td>
                                    <td>Total</td>
                                    <td>#</td>
                                    <td>#</td>
                                </tr>
                            </thead>
                            {{-- <tbody>
                            <tr class="tr_body_clone">
                                <td>
                                    <select name="store_id" id="store_ids" class="form-control" tabindex="-1" style="display: none; width: 100%">
                                        <option value="">--Pilih Toko-- </option>
                                        @foreach ($store as $item)
                                            <option  name="store_id" value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" name="harga_pabrik" id="harga_pabrik" class="form-control form-control-sm" />
                                </td>
                                <td>
                                    <input type="text" name="harga_distributor" id="harga_distributor" class="form-control form-control-sm" />
                                </td>
                                <td>
                                    <input type="text" name="qty" id="qty" class="form-control form-control-sm" />
                                </td>
                                <td>
                                    <input type="text" name="total" id="total_item" class="form-control form-control-sm" />
                                </td>
                                <td><input type="button" class="btn btn-danger btn-sm" id="ibtnDel"  value="-"></td>
                                <td colspan="5" style="text-align: left;">
                                    <input type="button" class="btn btn-warning btn-sm addRow" id="addrow" value="+" />
                                </td>
                            </tr>
                        </tbody> --}}
                            <tbody>

                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="4">Grand Total</td>
                                    <td>Rp <span id="grandtotal"></span></td>
                                    <td colspan="2"><span id="grandtotal"></span></td>
                                </tr>
                            </tfoot>
                        </table>
                        <h5 class="card-title">Item Produk</h5>

                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-warning" type="button" id="simpansemua">Simpan</button>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection
@push('after-scripts')
    <script src="{{ url('kimsin-theme/assets/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function() {
            $('#store_ids').select2();

            // untuk atas
            $("#produk").change(function () { 
                // alert('clicked');
                var produk = $(this).val();
                console.log(produk);
                $.ajax({
                    url: "{{route('produk.detail-item', ':id')}}".replace(':id', produk),
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        $('#hrgpabrik').val(data.harga_pabrik);
                        $('#hrgdis').val(data.harga_distributor);
                        $('#kode_produk').val(data.kode_product);
                    }
                });
                
            });

            $(document).on("click", "#addTable", function() {
                console.log('item masuk klik');
                var idprod = $("#produk").val();
                var kodeprod = $("#kode_produk").val();
                var produkId = $("#produk option:selected").text();

                var hargapabrik = $("#hrgpabrik").val();
                var hargadis = $("#hrgdis").val();
                var qty = $("#qtyitem").val();
                var totalitem = hargadis * qty;

                var tr = '<tr>  <td>' + produkId + ' <input type="hidden" id="idprod_item"  value='+idprod+'> <input type="hidden" id="kodeprod_item"  value='+kodeprod+'> </td>     <td>' + hargapabrik + ' <input type="hidden" id="hargapabrik_item"  value='+hargapabrik+'> </td>   <td>' +hargadis+ ' <input type="hidden" id="hargadis_item"  value='+hargadis+'> </td>   <td id="qty_untuk_po">' + qty + '<input type="hidden" id="qty_item"  value='+qty+'> </td>   <td id="totalItem">' + totalitem +' <input type="hidden" id="totalitem_item"  value='+totalitem+'> </td>   <td><input type="button" class="btn btn-danger btn-sm" id="ibtnDelTable"  value="-"></td></tr>';
                $("#myTable tbody").append(tr);

                sumtotalitem();
                totalQtyPo();
            });

            $("table.order-list").on("click", "#ibtnDelTable", function(event) {
                $(this).closest("tr").remove();
                sumtotalitem();
                totalQtyPo();
            });

            $("#simpansemua").click(function () { 
                // po
                var id          = $("#purchase_id").val();
                var kodepo      = $("#kodepo_po").val();
                var store_id    = $("#store_id_po").val();
                var tanggal_po  = $("#tanggal_po").val();
                var jatuh_tempo = $("#jatuh_tempo_po").val();
                var supplier    = $("#supplier_po").val();
                var qty_po      = $("#total_qty_tambah").val();
                var total_po    = $("#jumlah_total_po_tambah").val();
                var dp_po       = $("#dp_po_tambahan").val();
                var sisa        = $("#sisa_po_tambahan").val();
                console.log(kodepo, 'store=',store_id, 'tgl-po=',tanggal_po, 'jthtemp=',jatuh_tempo, 'sup=',supplier, 'qtypo=',qty_po, 'totpo=',total_po, 'dppo=',dp_po, 'sisa=',sisa);
                // console.log('----');
                // item
                var kode_invoice    = $("#kodeinv").val();
                var tanggal_invoice = $("#tanggalinv").val();
                
                let kodeprod_item = [];
                let idprod_item = [];
                let hargapabrik_item = [];
                let hargadis_item = [];
                let qty_item = [];
                let totalitem_item = [];

                $('table.order-list #kodeprod_item').each(function () {
                    kodeprod_item.push($(this).val());
                });
                $('table.order-list #idprod_item').each(function () {
                    idprod_item.push($(this).val());
                });
                $('table.order-list #hargapabrik_item').each(function () {
                    hargapabrik_item.push($(this).val());
                });
                $('table.order-list #hargadis_item').each(function () {
                    hargadis_item.push($(this).val());
                });
                $('table.order-list #qty_item').each(function () {
                    qty_item.push($(this).val());
                });
                $('table.order-list #totalitem_item').each(function () {
                    totalitem_item.push($(this).val());
                });

                // console.log(kode_invoice, 'tglinv=',tanggal_invoice, 'produkid=',idprod, 'hrgpbrik=',hargapabrik, 'hrgdis',hargadis, 'qtyitem=',qty_item, 'totitem=',totalitem);
                $.ajax({
                    url: "{{ route('purchase.item-po.update') }}",
                    method: 'post',
                    data: {
                        _token: CSRF_TOKEN,
                        purchase_id: id,
                        kode_po: kodepo,
                        store_id: store_id,
                        tanggal_po: tanggal_po,
                        jatuh_tempo: jatuh_tempo,
                        supplier: supplier,
                        qty_po: qty_po,
                        total_po: total_po,
                        dp_po: dp_po,
                        sisa: sisa,
                        kode_invoice: kode_invoice,
                        tanggal_invoice: tanggal_invoice,
                        product_id: idprod_item,
                        kode_product: kodeprod_item,
                        harga_pabrik: hargapabrik_item,
                        harga_distributor: hargadis_item,
                        qty_item: qty_item,
                        totalitem: totalitem_item,
                    },
                    success: function(data) {
                        console.log(data);
                        window.location.reload("{{ route('purchase.index') }}") ;
                    }
                });
                
                
            });

        });

        function sumtotalitem() {
            var total = 0;
            var getValueTotal = $("#total_po").val();
            var total_tambahan = 0;
            $("#myTable tbody tr").each(function() {
                var totalitem = $(this).find('#totalItem').text();
                total += +totalitem;
            });
            console.log(total);
            $("#grandtotal").text(total.toFixed(2));
            $("#total_po_tambah").val(total);

            total_tambahan = parseInt(total) + parseInt(getValueTotal);
            $("#jumlah_total_po_tambah").val(total_tambahan);
        }
        
        function totalQtyPo() {
            var totalQty = 0;
            var getValueQty = $("#qty_po").val() ;
            var addQtyItem = 0;
            $("#myTable tbody tr").each(function() {
                var totalQtyItem = $(this).find('#qty_untuk_po').text();
                totalQty += +totalQtyItem;
            });
            
            console.log('qty',totalQty);
            $("#qty_po_tambah").val(totalQty);
            
            addQtyItem = parseInt(totalQty) + parseInt(getValueQty);

            $("#total_qty_tambah").val(addQtyItem);
            console.log('hasil',addQtyItem);
            
        }

        

        // function calculateRow(row) {
        //     var price = +row.find('#harga_distributor').val();
        // }

        // function calculateGrandTotal() {
        //     var grandTotal = 0;

        //     $("table.order-list").find('input[id^="total_item"]').each(function () {
        //         // grandTotal += +$(this).val();
        //         var total = $(this).val()
        //         grandTotal += +total;
        //         // console.log('total',total[i]);
        //     });
        //     console.log('=',grandTotal);
        //     $("#grandtotal").text(grandTotal.toFixed(2));
        // }

        $("#hitung-po").keyup(function(e) {
            var total = $("#jumlah_total_po_tambah").val();
            var sisa_db = $("#dp_po").val();
            var dp = $("#dp_po_tambahan").val();
            var sisa = parseInt(total) - parseInt(sisa_db) - parseInt(dp);
            $("#sisa_po_tambahan").attr("value", sisa);
        });
    </script>
@endpush
