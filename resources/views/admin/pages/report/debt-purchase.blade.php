@extends('admin.layouts.layout-dashboard')
@section('content')
    <!-- Page-header end -->

    <div class="page-info">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Extended</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data Tables</li>
            </ol>
        </nav>
    </div>
    @if (session('success'))
        <div class="alert alert-primary outline-alert" role="alert">
            {{ session('success') }}
            {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button> --}}
        </div>
    @endif
    <div class="main-wrapper">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Data Piutang</h5>
                        <br>
                        <table id="zero-conf" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Invoice</th>
                                    <th>Toko</th>
                                    <th>Tanggal</th>
                                    <th>Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="accordionSummaryPR">
                                @foreach ($debt as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->kode_invoice }}</td>
                                        <td>{{ $item->store->name ?? '' }}</td>
                                        <td>{{ $item->tanggal_invoice }}</td>
                                        <td>1</td>
                                        <td>
                                            <button class="btn btn-primary" data-toggle="collapse"
                                                data-target="#collapse{{ $item->id }}" aria-expanded="true"
                                                aria-controls="collapse{{ $item->id }}">Expand Detail</button>
                                        </td>
                                    </tr>
                                    <td colspan="6">
                                        <div id="collapse{{ $item->id }}" class="collapse" aria-labelledby="headingOne"
                                            data-parent="#accordionSummaryPR">
                                            <div class="card-body p-1">
                                                <b>Detail Item</b>
                                                <div class="table-responsive">
                                                    <table class="table default-table">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Produk</th>
                                                                <th>Jumlah</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php
                                                                $sumtotal_bayar = 0;
                                                            @endphp
                                                            @foreach ($item->purchase_item as $row)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>{{ $row->product->name ?? '' }}</td>
                                                                    <td>{{ $row->qty }}</td>
                                                                    <td>@currency($row->total)</td>
                                                                </tr>
                                                                @php
                                                                    $sumtotal_bayar += $row->total;
                                                                @endphp
                                                            @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="3">Total</td>
                                                                <td>@currency($sumtotal_bayar)</td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('after-scripts')
@endpush
