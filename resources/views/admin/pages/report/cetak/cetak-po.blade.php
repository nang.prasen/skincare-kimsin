<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet">
</head>

<body>
    <div class="container">

        {{-- <div class="page-header">
            <h1>Invoice Template </h1>
        </div> --}}


        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 body-main">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                
                                <h5 style="color: #F81D2D;"><strong>{{$surat_po->kode_po ?? ''}}</strong></h5>
                                
                            </div>
                        </div>
                        <br />
                        <br />
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            <h6>No</h6>
                                        </th>
                                        <th>
                                            <h6>Item</h6>
                                        </th>
                                        <th><h6>Jumlah</h6></th>
                                        <th><h6>Harga Pabrik</h6></th>
                                        <th>
                                            <h6>Total</h6>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($item_po as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->name_product}}</td>
                                            <td>{{$item->qty_total}}</td>
                                            <td>@currency($item->harga_pabrik)</td>
                                            <td>@currency($item->total_rupiah)</td>
                                        </tr>
                                    @endforeach
                                    
                                    <tr>
                                        <td class="text-right" colspan="4">
                                            <p>
                                                <strong>Grant Total</strong>
                                            </p>
                                            <p>
                                                <strong>DP: </strong>
                                            </p>
                                            <p>
                                                <strong>Sisa: </strong>
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                <strong>@currency($surat_po->total)</strong>
                                            </p>
                                            <p>
                                                <strong>@currency($surat_po->dp)</strong>
                                            </p>
                                            <p>
                                                <strong>@currency($surat_po->sisa)</strong>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr style="color: #F81D2D;">
                                        <td class="text-right" colspan="4">
                                            <h5><strong>Total:</strong></h5>
                                        </td>
                                        <td class="text-left">
                                            <h5><strong>@currency($surat_po->total)</strong></h5>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <div class="col-md-12">
                                <p><b>Date :</b> {{$surat_po->tanggal_po}}</p>
                                <br />
                                <p><b>Signature</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- surat jalan --}}
            @foreach ($surat_inv as $inv)
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 body-main">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">

                                    <h5 style="color: #F81D2D;"><strong>{{ $inv->kodeinv ?? ''}}</strong></h5>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 style="color: #F81D2D;"><strong>{{ $inv->store_name ?? '' }}</strong></h5>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <h6>No</h6>
                                            </th>
                                            <th>
                                                <h6>Item</h6>
                                            </th>
                                            <th>
                                                <h6>Jumlah</h6>
                                            </th>
                                            <th>
                                                <h6>Harga Distributor</h6>
                                            </th>
                                            <th>
                                                <h6>Total</h6>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @php
                                            $total_harga = 0;
                                        @endphp --}}
                                        {{-- @foreach ($surat_jalan->purchase_item as $item) --}}
                                            <tr>
                                                <th>{{ $loop->iteration }}</th>
                                                <td>{{ $inv->name_products ?? '' }}</td>
                                                <td>{{ $inv->qty_item }}</td>
                                                <td>@currency((float)$inv->hrg_item ?? '')</td>
                                                <td>@currency($inv->total_item)</td>
                                            </tr>
                                            {{-- @php
                                                $total_harga += $item->total;
                                            @endphp
                                        @endforeach --}}
                                        <tr>
                                            <td class="text-right" colspan="4">
                                                <p>
                                                    <strong>Grant Total</strong>
                                                </p>
                                            </td>
                                            <td>
                                                <p>
                                                    {{-- <strong>@currency($total_harga)</strong> --}}
                                                </p>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <div class="col-md-12">
                                    <p><b>Date :</b> {{ $inv->tgl_inv }}</p>
                                    <br />
                                    <p><b>Signature</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="text-center">
                    <a class="btn btn-primary btn-xs" href="{{route('cetak.pdf.po',$surat_po->id)}}">cetak</a>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .body-main {
            background: #ffffff;
            border-bottom: 15px solid #1E1F23;
            border-top: 15px solid #1E1F23;
            margin-top: 30px;
            margin-bottom: 30px;
            padding: 40px 30px !important;
            position: relative;
            box-shadow: 0 1px 21px #808080;
            font-size: 10px;



        }

        .main thead {
            background: #1E1F23;
            color: #fff;
        }

        .img {
            height: 100px;
        }

        h1 {
            text-align: center;
        }
    </style>
</body>

</html>


{{-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--  This file has been downloaded from bootdey.com @bootdey on twitter -->
    <!--  All snippets are MIT license http://bootdey.com/license -->
    <title>Purchase Order - {{$surat_po->kode_po}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="row">
            <!-- BEGIN INVOICE -->
            <div class="col-xs-12">
                <div class="grid invoice">
                    <div class="grid-body">
                        <div class="invoice-title">
                            <div class="row">
                                <div class="col-xs-12">
                                    <img src="http://vergo-kertas.herokuapp.com/assets/img/logo.png" alt=""
                                        height="35">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2>Purchase Order<br>
                                        <span class="small">{{$surat_po->kode_po}}</span>
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-6">
                                <address>
                                    <strong>Pembelian :</strong><br>
                                    PT Produksi Skincare<br>
                                    Slemen, Yogyakarta<br>
                                    <abbr title="Phone">P:</abbr> (123) 456-7890
                                </address>
                            </div>
                            <div class="col-xs-6 text-right">
                                <address>
                                    <strong>Dikirim :</strong><br>
                                    Kimsin Beauty Indonesia<br>
                                    Jl. Malioboro No 1<br>
                                    Yogyakarta<br>
                                    <abbr title="Phone">P:</abbr> (123) 345-6789
                                </address>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <address>
                                    <strong>Tanggal Order:</strong><br>
                                    {{$surat_po->tanggal_po}}<br>
                                </address>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>RINCIAN</h3>
                                <table class="table table-striped">
                                    <thead>
                                        <tr class="line">
                                            <td><strong>#</strong></td>
                                            <td class="text-center"><strong>DESKRIPSI</strong></td>
                                            <td class="text-center"><strong>QTY</strong></td>
                                            <td class="text-right"><strong>SUBTOTAL</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><strong>Skincare</strong><br>Pembelian Skincare</td>
                                            <td class="text-center">{{$surat_po->qty}}</td>
                                            <td class="text-right">@currency($surat_po->total)</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-right"><strong>Taxes</strong></td>
                                            <td class="text-right"><strong>N/A</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                            </td>
                                            <td class="text-right"><strong>Total</strong></td>
                                            <td class="text-right"><strong>@currency($surat_po->total)</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right identity">
                                <a class="btn btn-primary" href="{{route('cetak.pdf.po',$surat_po->id)}}">cetak</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END INVOICE -->
        </div>
    </div>

    <style type="text/css">
        body {
            margin-top: 20px;
            background: #eee;
        }

        @page {
    size: 7in 9.25in;
    margin: 27mm 16mm 27mm 16mm;
}

        .invoice {
            padding: 30px;
        }

        .invoice h2 {
            margin-top: 0px;
            line-height: 0.8em;
        }

        .invoice .small {
            font-weight: 300;
        }

        .invoice hr {
            margin-top: 10px;
            border-color: #ddd;
        }

        .invoice .table tr.line {
            border-bottom: 1px solid #ccc;
        }

        .invoice .table td {
            border: none;
        }

        .invoice .identity {
            margin-top: 10px;
            font-size: 1.1em;
            font-weight: 300;
        }

        .invoice .identity strong {
            font-weight: 600;
        }


        .grid {
            position: relative;
            width: 100%;
            background: #fff;
            color: #666666;
            border-radius: 2px;
            margin-bottom: 25px;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
        }
    </style>

    <script type="text/javascript"></script>
</body>

</html> --}}
