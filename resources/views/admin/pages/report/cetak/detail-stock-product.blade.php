
@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#">Laporan Stok</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Stok</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Toko : {{$store->name}}</h5>
                    <br>
                    <table id="myTable" class="table table-bordered order-list">
                        <thead>
                            <tr>
                                <td>Produk</td>
                                <td>Harga Distributor</td>
                                <td>Stok</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total_stock = 0;
                            @endphp
                            @foreach ($detail as $item)
                                @foreach ($stok_toko as $stok)
                                    <tr>
                                        <td>{{$item->product->name}}</td>
                                        <td>@currency($item->product->harga_distributor)</td>
                                        <td>{{$item->total_qty - $stok->stok_penjualan_toko}}</td>
                                    </tr>
                                @php
                                    $total_stock += $item->total_qty - $stok->stok_penjualan_toko;
                                @endphp
                                @endforeach
                            @endforeach
                        </tbody>
                    
                        <tfoot>
                            <tr>
                                <td colspan="2">Grand Total</td>
                                <td>{{$total_stock}}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>




@endsection
@push('after-scripts')
    
@endpush

