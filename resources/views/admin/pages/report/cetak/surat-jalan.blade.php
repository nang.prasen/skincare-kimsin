{{-- <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Export PDF Laravel 8!</title>
  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" style="background-color: bisque;">
                        <h6 class="card-title">Kode Po      : {{$surat_jalan->kode_invoice}}</h6> 
                        <h6 class="card-title">Tujuan/Toko      : {{$surat_jalan->store->name}}</h6> 
                        <h6 class="card-title">Tanggal Po   : {{$surat_jalan->tanggal_invoice}}</h6> 
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title">Data Item</p>
                        <table class="table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Total</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($surat_jalan->purchase_item as $item)    
                                    <tr>
                                        <th>{{$loop->iteration}}</th>
                                        <td>{{$item->product->name}}</td>
                                        <td>{{$item->qty}}</td>
                                        <td>@currency($item->total)</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html> --}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet">
</head>

<body>
    <div class="container">

        {{-- <div class="page-header">
            <h1>Invoice Template </h1>
        </div> --}}


        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 body-main">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">

                                <h5 style="color: #F81D2D;"><strong>{{ $surat_jalan->kode_invoice }}</strong></h5>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5 style="color: #F81D2D;"><strong>{{ $surat_jalan->store->name }}</strong></h5>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            <h6>No</h6>
                                        </th>
                                        <th>
                                            <h6>Item</h6>
                                        </th>
                                        <th>
                                            <h6>Jumlah</h6>
                                        </th>
                                        <th>
                                            <h6>Harga Distributor</h6>
                                        </th>
                                        <th>
                                            <h6>Total</h6>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total_harga = 0;
                                    @endphp
                                    @foreach ($surat_jalan->purchase_item as $item)
                                        <tr>
                                            <th>{{ $loop->iteration }}</th>
                                            <td>{{ $item->product->name ?? '' }}</td>
                                            <td>{{ $item->qty }}</td>
                                            <td>@currency($item->harga_distributor ?? '')</td>
                                            <td>@currency($item->total)</td>
                                        </tr>
                                        @php
                                            $total_harga += $item->total;
                                        @endphp
                                    @endforeach
                                    <tr>
                                        <td class="text-right" colspan="4">
                                            <p>
                                                <strong>Grant Total</strong>
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                <strong>@currency($total_harga)</strong>
                                            </p>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <div>
                            <div class="col-md-12">
                                <p><b>Date :</b> {{ $surat_jalan->tanggal_invoice }}</p>
                                <br />
                                <p><b>Signature</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .body-main {
            background: #ffffff;
            border-bottom: 15px solid #1E1F23;
            border-top: 15px solid #1E1F23;
            margin-top: 30px;
            margin-bottom: 30px;
            padding: 40px 30px !important;
            position: relative;
            box-shadow: 0 1px 21px #808080;
            font-size: 10px;



        }

        .main thead {
            background: #1E1F23;
            color: #fff;
        }

        .img {
            height: 100px;
        }

        h1 {
            text-align: center;
        }
    </style>
</body>

</html>
