@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Extended</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Tables</li>
        </ol>
    </nav>
</div>
@if (session('success'))
<div class="alert alert-primary outline-alert" role="alert">
    {{ session('success') }}
    {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button> --}}
</div>
@endif
<div class="main-wrapper">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Piutang</h5>
                    <br>
                    <table id="zero-conf" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode PO</th>
                                <th>Tanggal</th>
                                <th>Jatuh Tempo</th>
                                <th>Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="accordionSummaryPR">
                            @foreach ($piutang as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->kode_po}}</td>
                                    <td>{{$item->tanggal_po}}</td>
                                    <td>{{$item->jatuh_tempo}}</td>
                                    <td>@currency($item->total)</td>
                                    <td>
                                        <button class="btn btn-primary" data-toggle="collapse" data-target="#collapse{{ $item->id }}" aria-expanded="true" aria-controls="collapse{{ $item->id }}">Expand Detail</button>
                                    </td>
                                </tr>   
                                <td colspan="6">
                                    <div id="collapse{{ $item->id }}" class="collapse" aria-labelledby="headingOne"
                                        data-parent="#accordionSummaryPR">
                                        <div class="card-body p-1">
                                            <div class="row">
                                                <div class="col">
                                                    <b>Kode PO   : </b>{{ $item->kode_po }}<br>
                                                    <b>Tgl PO    : </b>{{ $item->tanggal_po }}<br>
                                                    <b>Jth Tempo : </b>{{ $item->jatuh_tempo }}
                                                </div>
                                                <div class="col">
                                                    <b>Total     : </b>@currency($item->total)<br>
                                                    <b>DP        : </b>@currency($item->dp)<br>
                                                    <b>Sisa      : </b>@currency($item->sisa)<br>
                                                </div>
                                            </div>
                                            <hr>
                                            {{-- <button class="btn btn-success text-left status_pembatalan mb-2">Edit Status Pembatalan</button> --}}
                                            {{-- <br> --}}
                                            <b>Purchase Pembayaran</b>
                                            <div class="table-responsive">
                                                <table class="table default-table">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Tanggal Bayar</th>
                                                            <th>Total Bayar</th>
                                                            <th>Keterangan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $sumtotal_bayar = 0;
                                                        @endphp
                                                        @foreach ($item->purchase_pay as $row)
                                                            <tr>
                                                                <td>{{$loop->iteration}}</td>
                                                                <td>{{$row->tanggal_pay}}</td>
                                                                <td>@currency($row->total_pay)</td>
                                                                <td>{{$row->keterangan_pay}}</td>
                                                            </tr>
                                                            @php
                                                                $sumtotal_bayar += $row->total_pay;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="2">Total</td>
                                                            <td>@currency($sumtotal_bayar)</td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </td> 
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>




@endsection
@push('after-scripts')
    
@endpush

