@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Laporan Stok</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body" style="background-color: #D4F6CC;">
                    {{-- <h5 class="card-title">Produk  : {{$item->name}}</h5> 
                    <h5 class="card-title">Total   : {{$item->qty}}</h5>  --}}
                    <table id="table-stock" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Produk</th>
                                <th>Jumlah Stok</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($produk as $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->qty}}</td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Stok</h5>
                    <br>
                    <table id="zero-conf" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Toko</th>
                                {{-- <th>Stok Masuk</th>
                                <th>Stok Keluar</th> --}}
                                <th>Jumlah Stok</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($stock as $item)
                                @foreach ($sale as $s)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$item->store_name}}</td>
                                        <td>{{$item->qty_total}}</td>
                                        {{-- @if ($item->product_id == $s->product_id)
                                            
                                            <td>{{$s->penjualan}}</td>
                                            <td>{{$item->qty_total }}</td>
                                        @else
                                            <td></td>
                                            <td>{{$item->qty_total }}</td>
                                        @endif --}}
                                        <td>
                                            <a href="{{route('report.detail-stock',$item->idstore)}}" class="btn btn-primary btn-xs" >Detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>




@endsection
@push('after-scripts')
    
@endpush

