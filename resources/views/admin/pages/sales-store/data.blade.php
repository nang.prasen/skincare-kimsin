@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Extended</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data Tables</li>
        </ol>
    </nav>
</div>
@if (session('success'))
<div class="alert alert-primary outline-alert" role="alert">
    {{ session('success') }}
    {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button> --}}
</div>
@endif
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <a href="{{route('sale-store.create')}}" class="btn btn-primary btn-sm"  style="float: right;">input penjualan</a>
                {{-- <p class="page-desc">DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, built upon the foundations of progressive enhancement, that adds many advanced features to any HTML table.</p> --}}
            </div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Penjualan</h5>
                    <div class="row">
                        <div class="col-md-5">

                            <input type="date" class="form-control" id="start_date" value="{{$start_date}}" name="start_date">
                        </div>
                        <div class="col-md-5">

                            <input type="date" class="form-control" id="end_date" value="{{$end_date}}" name="end_date">
                        </div>
                        {{-- <div class="col-md-2">
                            <button type="button" class="btn btn-info">filter</button>
                        </div> --}}
                    </div>
                    <br>
                    <div id="data_sale_store">

                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>




@endsection
@push('after-scripts')
    <script>
        loadSale();
        $("#start_date,#end_date").on("change", function () {
            // console.log('ok');
            loadSale();
        });
        function loadSale() {
            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();
            $("#data_sale_store").load("{{route('sale-store.index',['key'=>'filter_date']) }}&start_date="+start_date+"&end_date="+end_date);
        }
    </script>
@endpush

