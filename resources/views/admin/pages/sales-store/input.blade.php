@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Data Penjualan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <p class="page-desc">Input data dengan benar</p>
            </div>
        </div>
    </div>
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible fade show bg-danger text-white" role="alert"">
            {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif
    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Input Penjualan</h5>
                    <form action="{{route('sale-store.store')}}" method="POST">
                        @csrf
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">No Faktur</label>
                            <input type="text" name="kode_sale" class="form-control" value="{{$kode}}" readonly id="kode_sale" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <div class="form-group">
                            <label>Toko</label>
                            <select name="inv_id" id="inv_id" class="js-states form-control" tabindex="-1"
                                style="display: none; width: 100%">
                                <option value="">--Pilih Toko-- </option>
                                @foreach ($toko as $item)
                                    <option name="inv_id" value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- <div class="form-group">
                            <label>Toko</label>
                            <select name="inv_id" id="inv_id" class="js-states form-control" tabindex="-1"
                                style="display: none; width: 100%">
                                <option value="">--Pilih Invoice-- </option>
                                @foreach ($invoice as $item)
                                    <option name="inv_id" value="{{ $item->kode_invoice }}">{{ $item->kode_invoice }}</option>
                                @endforeach
                            </select>
                        </div> --}}
                        <hr>
                        <div class="row">
                            <input type="hidden" id="storeId" name="store_id">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h5> Distributor : <span id="namaToko"></span> </h5>
                                </div>
                                <table class="table table-bordered sales-list" id="tableSales">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Produk</th>
                                            <th>Harga</th>
                                            <th>Jumlah</th>
                                            <th>Penjualan</th>
                                            <th>#</th>
                                        </tr>
                                    </thead><tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Penjualan</label>
                            <input type="date" name="tanggal_sale" class="form-control" id="tanggal_sale" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}" />


@endsection
@push('after-scripts')
<script src="{{ url('kimsin-theme/assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("#produk").change(function () { 
                // alert('clicked');
        var produk = $(this).val();
        console.log(produk);
        $.ajax({
            url: "{{route('produk.detail-item', ':id')}}".replace(':id', produk),
            type: "GET",
            dataType: "json",
            success: function (data) {
                // console.log(data);
                $('#hargadistributor').val(data.harga_distributor);
            }
        });
        
    });
    $(document).ready(function () {
        $('select[name="inv_id"]').change(function (e) { 
            e.preventDefault();
            var kode = $(this).val();
            // console.log(kode);
            if (kode) {
                    $.ajax({
                    type: "get",
                    url: "{{route('product.detail-purchase')}}",
                    data: {store_id: kode},
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        $('.sales-list tbody').empty();
                        $.each(response.produk, function (key, value) { 
                            $('.sales-list tbody').append(
                                '<tr>'+
                                    '<td>'+value.product_id+' </td>'+
                                    '<td id="produkId">'+value.nama_produk+'</td>'+
                                    '<td id="hargadistributor_'+value.product_id+'">'+value.harga_distributor+'</td>'+
                                    '<td>'+value.total_qty+'</td>'+
                                    '<td><input type="number" name="qty_retur[]" id="qty_retur_'+value.product_id+'" class="form-control"  > <input type="hidden" name="produkId[]" value="'+value.product_id+'" class="form-control" > <input type="hidden" name="purchaseItemId[]" value="'+value.id+'" class="form-control" > <input type="hidden" name="harga_distributor[]" value="'+value.harga_distributor+'" class="form-control" > <input type="hidden" name="kode_inv" value="'+value.kode_invoice+'" class="form-control" >   </td>'+
                                    '<td><input class="form-check-input pilih_item" type="checkbox" value="'+value.product_id+'">   </td>'+
                                '</tr>');
                        });
    
                        $('#storeId').val(response['produk'][0].store_id);
                        $('#namaToko').text(response['produk'][0].nama_store);
    
                        
                    }
                });
            } else {
                $('.retur-list tbody').empty();
            }
                
        });
    });

    function saleInput() { 
        // alert('clicked');
        let checkbox_pilih = $("#tableSales tbody .pilih_item:checked");
        let semua_id = [];
        $.each(checkbox_pilih, function (index, elm) { 
            semua_id.push(elm.value)
        });
        console.log(semua_id);
        let result = [];
        $.each(semua_id, function (index, value) { 
            let qty     = $(`#qty_retur_${value}`).val();
            let harga   = $(`#hargadistributor_${value}`).text();
            let simpan  = {
                "qty": qty,
                "produk": value,
                "harga": harga
            }     
            result.push(simpan);
            // console.log(qty);    
        });
        console.log(result);

        $.ajax({
            type: "post",
            url: "{{route('sale-store.store')}}",
            data: {
                _token: CSRF_TOKEN,
                item_sale: result,
                kode_sale: $('#kode_sale').val(),
                kode_invoice: $('#inv_id').val(),
                tanggal_sale: $('#tanggal_sale').val(),
                store_id: $('#storeId').val(),

            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                alert('Berhasil disimpan');
                window.location.href = "{{route('sale-store.index')}}";
            }
        });
     }
</script>
@endpush

