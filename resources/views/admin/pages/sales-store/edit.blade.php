@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Edit Penjualan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <p class="page-desc">Input data dengan benar</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Input Penjualan</h5>
                    <form action="{{route('sale-store.update',$sales_store->id)}}" method="POST">
                        @method('put')
                        @csrf
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">No Faktur</label>
                            <input type="text" name="kode_sale" class="form-control" value="{{$sales_store->kode_sale}}" readonly id="kode_sale" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label>Kode Inv</label>
                                    <br>
                                    <b>{{ $sales_store->kode_invoice }}</b>
                                    <input name="id_sale_store" class="form-control" value="{{ $sales_store->id }}" type="hidden">
                                    <input name="kode_invoice" class="form-control" value="{{ $sales_store->kode_invoice }}" type="hidden">
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label>Distributor</label>
                                    <br>
                                    <b>{{$sales_store->store->name}}</b>
                                    <input name="store_id" class="form-control" value="{{$sales_store->store->id}}" type="hidden">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <input type="hidden" id="storeId">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{-- <h5> Distributor : <span id="namaToko">{{$sales_store->store->name}}</span> </h5> --}}
                                </div>
                                <table class="table table-bordered sales-list" id="tableSales">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Produk</th>
                                            <th>Harga</th>
                                            <th>Jumlah</th>
                                            <th>Edit</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sales_store->sale_item as $item)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                @foreach ($item->product as $row)
                                                    
                                                <td>{{$row->name}}</td>
                                                @endforeach
                                                <td>{{$item->harga_distributor}}</td>
                                                <td>{{$item->qty_sale}}</td>
                                                <td>
                                                    <input type="number" name="qty_retur[]" class="form-control"  >
                                                    <input type="hidden" name="iditem[]" value="{{$item->id}}">
                                                </td>
                                                <td><input class="form-check-input pilih_item" type="checkbox"></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Penjualan</label>
                            <input type="date" name="tanggal_sale" class="form-control" value="{{$sales_store->tanggal_sale}}" id="tanggal_sale" readonly>
                        </div>
                        {{-- <button type="submit" onclick="saleUpdate()" class="btn btn-primary">Update</button> --}}
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}" />


@endsection
@push('after-scripts')
<script src="{{ url('kimsin-theme/assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $("#produk").change(function () { 
                // alert('clicked');
        var produk = $(this).val();
        console.log(produk);
        $.ajax({
            url: "{{route('produk.detail-item', ':id')}}".replace(':id', produk),
            type: "GET",
            dataType: "json",
            success: function (data) {
                // console.log(data);
                $('#hargadistributor').val(data.harga_distributor);
            }
        });
        
    });

    // function saleUpdate() { 
    //     // alert('clicked');
    //     let checkbox_pilih = $("#tableSales tbody .pilih_item:checked");
    //     let semua_id = [];
    //     $.each(checkbox_pilih, function (index, elm) { 
    //         semua_id.push(elm.value)
    //     });
    //     console.log(semua_id);
    //     let result = [];
    //     $.each(semua_id, function (index, value) { 
    //         let qty     = $(`#qty_retur_${value}`).val();
    //         let harga   = $(`#hargadistributor_${value}`).text();
    //         let update  = {
    //             "qty": qty,
    //         }     
    //         result.push(update);
    //         // console.log(qty);    
    //     });
    //     console.log(result);

    //     $.ajax({
    //         type: "post",
    //         url: "{{route('sale-store.update',':id')}}".replace(':id', produk),
    //         // url = url.replace(':id', layer_id );
    //         data: {
    //             _token: CSRF_TOKEN,
    //             item_sale_update: result,
    //             // kode_sale: $('#kode_sale').val(),
    //             // kode_invoice: $('#inv_id').val(),
    //             // tanggal_sale: $('#tanggal_sale').val(),
    //             // store_id: $('#storeId').val(),

    //         },
    //         dataType: "json",
    //         success: function (response) {
    //             console.log(response);
    //             alert('Berhasil disimpan');
    //             window.location.href = "{{route('sale-store.index')}}";
    //         }
    //     });
    //  }
</script>
@endpush

