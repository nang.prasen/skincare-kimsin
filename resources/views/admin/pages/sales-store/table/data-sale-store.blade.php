<table id="zero-conf" class="display" style="width:100%">
    <thead>
        <tr>
            <th>No</th>
            <th>No Faktur</th>
            <th>Invoice</th>
            <th>Toko</th>
            <th>Tanggal</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($penjualan as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->kode_sale }}</td>
                <td>{{ $item->kode_invoice }}</td>
                <td>{{ $item->store->name ?? '' }}</td>
                <td>{{ $item->tanggal_sale }}</td>
                <td>
                    <a href="{{ route('sale-store.edit', $item->id) }}" class="btn btn-secondary btn-sm">Edit</a>
                    {{-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Hapus</button> --}}
                    <form action="{{ route('sale-store.destroy', $item->id) }}" method="POST"
                        style="display: inline-block;">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm" value="Delete"
                            onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data {{ $item->no_retur }} ?')">
                            Hapus
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach

    </tbody>
</table>
<div id="paginate_sale">
    {{ $penjualan->appends($_GET)->links() }}
</div>
<script>
    $('#paginate_sale .pagination a').on('click', function(e) {
        e.preventDefault();

        url = $(this).attr('href');
        $.ajax({
            url: url,
            method: "GET",
            success: function(response) {
                $('#data_sale_store').html(response);
            }
        });
    });
</script>