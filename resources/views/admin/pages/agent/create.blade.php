@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Data Toko</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
    </nav>
</div>
@if (session('error'))
<div class="alert alert-danger outline-alert" role="alert">
    {{ session('error') }}
    {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button> --}}
</div>
@endif
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <p class="page-desc">Input data dengan benar</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Input Agen Toko</h5>
                    <form action="{{route('agent.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Toko</label>
                            <select name="store_id" id="store_id" class="js-states form-control" tabindex="-1" style="display: none; width: 100%">
                                <option value="">--Pilih Toko-- </option>
                                @foreach ($stores as $store)
                                    <option  name="store_id" value="{{$store->id}}">{{$store->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Agen</label>
                            <input type="text" name="name" class="form-control" aria-describedby="name" placeholder="Masukan nama agen toko">
                        </div>
                        <div class="form-group">
                            <label>Kode Agen</label>
                            <input type="text" name="kode_agent" value="{{$kode}}" readonly class="form-control" aria-describedby="name" placeholder="Masukan kode produk">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="address" class="form-control" id="" cols="10" rows="3"></textarea>
                            {{-- <input type="text" name="description" class="form-control" aria-describedby="name" placeholder="Masukan nama kategori"> --}}
                        </div>
                        <div class="form-group">
                            <label>Nomor HP</label>
                            <input type="number" name="phone" class="form-control" aria-describedby="name" placeholder="Masukan nomor agen toko">
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@push('after-scripts')
<script src="{{url('kimsin-theme/assets/plugins/select2/js/select2.full.min.js')}}"></script>
@endpush

