@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Data Kategori</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Edit</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <p class="page-desc">Edit data dengan benar</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Input Produk</h5>
                    <form action="{{route('product.update',$produk->id)}}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label>Nama Produk</label>
                            <input type="text" name="name" class="form-control" value="{{$produk->name}}">
                        </div>
                        <div class="form-group">
                            <label>Kode Produk</label>
                            <input type="text" name="kode_product" class="form-control" value="{{$produk->kode_product}}">
                        </div>
                        <div class="form-group">
                            <label>Kategori</label>
                            <select class="form-control" name="category_id">
                                <option>--Pilih Kategori--</option>
                                @foreach($category as $item)
                                <option name="category_id" value="{{$item->id}}" {{$produk->category_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="description" class="form-control" id="" cols="10" rows="3">{{$produk->description}}</textarea>
                            {{-- <input type="text" name="description" class="form-control" id="name" aria-describedby="name" placeholder="Masukan nama kategori"> --}}
                        </div>
                        <div class="form-group">
                            <label>Harga Modal</label>
                            <input type="number" name="harga_modal" class="form-control" value="{{$produk->harga_modal}}">
                        </div>
                        <div class="form-group">
                            <label>Harga Pabrik</label>
                            <input type="number" name="harga_pabrik" class="form-control" value="{{$produk->harga_pabrik}}">
                        </div>
                        <div class="form-group">
                            <label>Harga Distributor</label>
                            <input type="number" name="harga_distributor" class="form-control" value="{{$produk->harga_distributor}}">
                        </div>
                        <div class="form-group">
                            <label>Stok</label>
                            <input type="number" name="qty" class="form-control" value="{{$produk->qty}}">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

