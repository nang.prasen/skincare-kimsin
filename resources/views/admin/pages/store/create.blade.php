@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Data Toko</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <p class="page-desc">Input data dengan benar</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Input Toko</h5>
                    <form action="{{route('store.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Nama Toko</label>
                            <input type="text" name="name" class="form-control" aria-describedby="name" placeholder="Masukan nama toko">
                        </div>
                        <div class="form-group">
                            <label>Kode Toko</label>
                            <input type="text" name="kode_store" value="{{$kode}}" readonly class="form-control" aria-describedby="name" placeholder="Masukan kode produk">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="address" class="form-control" id="" cols="10" rows="3"></textarea>
                            {{-- <input type="text" name="description" class="form-control" aria-describedby="name" placeholder="Masukan nama kategori"> --}}
                        </div>
                        <div class="form-group">
                            <label>Nomor HP</label>
                            <input type="number" name="phone" class="form-control" aria-describedby="name" placeholder="Masukan nomor hp toko">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" aria-describedby="name" placeholder="Masukan email toko">
                        </div>
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" name="logo" class="form-control" aria-describedby="name" placeholder="Masukan stok">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

