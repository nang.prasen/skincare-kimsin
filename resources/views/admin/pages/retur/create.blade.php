@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Data Retur</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <p class="page-desc">Input data dengan benar</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Input Retur</h5>
                    <form action="{{route('retur.store')}}" method="POST">
                        @csrf
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">No Retur</label>
                            <input type="text" name="no_retur" class="form-control" value="{{$kode}}" readonly id="no_retur" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Tanggal Invoice</label>
                                    <input type="date" name="tanggal_inv" id="tanggalinv" class="form-control"
                                        aria-describedby="name" placeholder="Masukan nama kategori">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Cari Kode Invoice</label>
                                    <select name="kode_invoice" class="js-states form-control" tabindex="-1"
                                        style="display: none; width: 100%">
                                        <option value="">--Kode Invoice-- </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <input type="hidden" id="storeId">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h5 id="namaToko"></h5>
                                </div>
                                <table class="table table-bordered retur-list" id="tableRetur">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Produk</th>
                                            <th>Harga</th>
                                            <th>Jumlah</th>
                                            <th>Retur</th>
                                            <th>#</th>
                                        </tr>
                                    </thead><tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Retur</label>
                            <input type="date" name="tanggal_retur" class="form-control" id="tanggal_retur" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <button type="button" id="simpan-retur" onclick="simpanRetur()" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}" />

@endsection
@push('after-scripts')
<script src="{{ url('kimsin-theme/assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function() {
        
        $("#tanggalinv").change(function (e) { 
            e.preventDefault();
            // alert($(this).val());
            var tanggalinv = $(this).val();
            if (tanggalinv) {
                    $.ajax({
                    type: "get",
                    url: "{{route('tanggal.invoice')}}",
                    data: {tanggal_invoice: tanggalinv},
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        $('select[name="kode_invoice"]').empty();
                        $.each(response, function (key, value) { 
                            $('#kode_invoice').select2();
                            $('select[name="kode_invoice"]').append(
                                '<option value="'+value.kode_invoice+'" >'+value.kode_invoice+'</option>');
                        });
                    }
                });
            } else {
                $('select[name="kode_invoice"]').empty();
            }
            
        });

        $('select[name="kode_invoice"]').change(function (e) { 
            e.preventDefault();
            var kode = $(this).val();
            // console.log(kode);
            if (kode) {
                    $.ajax({
                    type: "get",
                    url: "{{route('product.detail-purchase')}}",
                    data: {kode_invoice: kode},
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        $('.retur-list tbody').empty();
                        $.each(response, function (key, value) { 
                            $('.retur-list tbody').append(
                                '<tr>'+
                                    '<td>'+value.product_id+' </td>'+
                                    '<td id="produkId">'+value.nama_produk+'</td>'+
                                    '<td>'+value.harga_distributor+'</td>'+
                                    '<td>'+value.qty+'</td>'+
                                    '<td><input type="number" name="qty_retur" id="qty_retur_'+value.product_id+'" class="form-control"  > </td>'+
                                    '<td><input class="form-check-input pilih_retur" type="checkbox" id="pilih_retur" value="'+value.product_id+'">   </td>'+
                                '</tr>');
                        });

                        $('#storeId').val(response[0].store_id);
                        $('#namaToko').text(response[0].nama_store);

                        
                    }
                });
            } else {
                $('.retur-list tbody').empty();
            }
            
        });

        // $("#simpan-retur").click(function () { 
        //     var no_retur = $('#no_retur').val();
        //     var tanggal = $('#tanggal_retur').val();
        //     var store = $('#storeId').val();
        //     var qty = $('#qty_retur').val();
        //     var produk = $('#produkId').text();
            
        // });
    });

    function simpanRetur(){
        let checkbox_pilih = $("#tableRetur tbody .pilih_retur:checked");
        let semua_id = [];
        $.each(checkbox_pilih, function (index, elm) { 
            semua_id.push(elm.value)
        });
        console.log(semua_id);
        let result = [];
        $.each(semua_id, function (index, value) { 
            let qty     = $(`#qty_retur_${value}`).val();
            let simpan  = {
                "qty": qty,
                "produk": value,
            }     
            result.push(simpan);
            // console.log(qty);    
        });
        console.log(result);

        $.ajax({
            type: "post",
            url: "{{route('retur.store')}}",
            data: {
                _token: CSRF_TOKEN,
                data_retur: result,
                no_retur: $('#no_retur').val(),
                tanggal_retur: $('#tanggal_retur').val(),
                store: $('#storeId').val(),

            },
            dataType: "json",
            success: function (response) {
                alert('ok');
            }
        });
    }
    
</script>
@endpush

