@extends('admin.layouts.layout-dashboard')
@section('content')


<!-- Page-header end -->

<div class="page-info">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Data Retur</a></li>
            <li class="breadcrumb-item active" aria-current="page">Form</li>
        </ol>
    </nav>
</div>
<div class="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <p class="page-desc">Edit data dengan benar</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Edit Retur</h5>
                    <form action="{{route('retur.update',$retur->id)}}" method="POST">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="exampleInputEmail1">No Retur</label>
                            <input type="text" name="no_retur" class="form-control" value="{{$retur->no_retur}}" readonly id="name" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Toko</label>
                                    <select name="store_id" id="store_id_po" class="js-states form-control" tabindex="-1"
                                        style="display: none; width: 100%">
                                        <option value="">--Pilih Toko-- </option>
                                        @foreach ($toko as $item)
                                            <option name="store_id" value="{{ $item->id }}" {{ $retur->store_id == $item->id ? 'selected' : '' }}  >{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Produk</label>
                                    <select name="product_id" class="js-states form-control" tabindex="-1"
                                        style="display: none; width: 100%">
                                        <option value="">--Pilih Produk-- </option>
                                        @foreach ($produk as $item)
                                            <option name="product_id" value="{{ $item->id }}" {{ $retur->product_id == $item->id ? 'selected' : ''}} >{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Retur</label>
                            <input type="date" name="tanggal_retur" class="form-control" value="{{$retur->tanggal_retur}}" id="name" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <div class="form-group">
                            <label>Jumlah Retur</label>
                            <input type="text" name="qty_retur" class="form-control" value="{{$retur->qty_retur}}" id="name" aria-describedby="name" placeholder="Masukan nama kategori">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@push('after-scripts')
<script src="{{ url('kimsin-theme/assets/plugins/select2/js/select2.full.min.js') }}"></script>
@endpush

