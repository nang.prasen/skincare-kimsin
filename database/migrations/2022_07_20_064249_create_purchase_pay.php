<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasePay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_pay', function (Blueprint $table) {
            $table->id();
            $table->integer('purchasing_id');
            $table->string('kode_pay');
            $table->string('tanggal_pay');
            $table->string('total_pay');
            $table->string('status_pay');
            $table->string('keterangan_pay')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_pay');
    }
}
