# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.18-MariaDB)
# Database: kimsin
# Generation Time: 2022-07-19 02:27:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table agents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `agents`;

CREATE TABLE `agents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int(11) NOT NULL,
  `kode_store` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'skincare','2022-07-18 22:40:16','2022-07-18 22:40:16'),
	(2,'fashion','2022-07-18 22:40:22','2022-07-18 22:40:22');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table invoices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoices`;

CREATE TABLE `invoices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode_invoice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchasing_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `tanggal_invoice` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;

INSERT INTO `invoices` (`id`, `kode_invoice`, `purchasing_id`, `store_id`, `tanggal_invoice`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'POD18072022-1001',1,1,'2022-07-19','2022-07-18 22:44:26','2022-07-18 22:44:26',NULL),
	(2,'POD18072022-1002',1,2,'2022-07-19','2022-07-18 22:53:02','2022-07-18 22:53:02',NULL);

/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),
	(4,'2019_08_19_000000_create_failed_jobs_table',1),
	(5,'2019_12_14_000001_create_personal_access_tokens_table',1),
	(6,'2022_07_01_001541_create_products',1),
	(7,'2022_07_01_002846_create_categories',1),
	(8,'2022_07_01_002922_create_store',1),
	(9,'2022_07_01_003026_create_agents',1),
	(10,'2022_07_01_075212_create_purchasing',1),
	(11,'2022_07_01_083055_create_purchase_item',1),
	(12,'2022_07_01_085416_create_sale_store',1),
	(13,'2022_07_10_100013_create_invoices',1),
	(14,'2022_07_12_075953_create_retur',1),
	(15,'2022_07_18_223751_create_retur_items',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table personal_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `personal_access_tokens`;

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_product` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_modal` int(11) NOT NULL,
  `harga_pabrik` int(11) NOT NULL,
  `harga_distributor` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `qty_retur` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `description`, `kode_product`, `harga_modal`, `harga_pabrik`, `harga_distributor`, `qty`, `qty_retur`, `category_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Facial Wash','Facial Wash','Facial Wash',21000,19000,24000,13,2,1,'2022-07-18 22:41:15','2022-07-18 23:20:56',NULL),
	(2,'Moisturizer','Moisturizer','Moisturizer',30000,25000,35000,27,4,1,'2022-07-18 22:41:46','2022-07-18 23:20:56',NULL),
	(3,'Hydrating Toner','Hydrating Toner','Hydrating Toner',32000,30000,42000,0,0,1,'2022-07-18 22:42:18','2022-07-18 22:42:18',NULL);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table purchase_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchase_item`;

CREATE TABLE `purchase_item` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `kode_product` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_pabrik` int(11) NOT NULL,
  `harga_distributor` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `purchase_item` WRITE;
/*!40000 ALTER TABLE `purchase_item` DISABLE KEYS */;

INSERT INTO `purchase_item` (`id`, `product_id`, `invoice_id`, `kode_product`, `qty`, `harga_pabrik`, `harga_distributor`, `total`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,'Facial',2,19000,24000,48000,'2022-07-18 22:44:26','2022-07-18 22:44:26',NULL),
	(2,2,1,'Moisturizer',5,25000,35000,175000,'2022-07-18 22:44:26','2022-07-18 22:44:26',NULL),
	(3,1,2,'Facial',11,19000,24000,264000,'2022-07-18 22:53:02','2022-07-18 22:53:02',NULL),
	(4,2,2,'Moisturizer',22,25000,35000,770000,'2022-07-18 22:53:02','2022-07-18 22:53:02',NULL);

/*!40000 ALTER TABLE `purchase_item` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table purchasing
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchasing`;

CREATE TABLE `purchasing` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode_po` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `tanggal_po` date NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `supplier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `dp` int(11) NOT NULL,
  `sisa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `purchasing` WRITE;
/*!40000 ALTER TABLE `purchasing` DISABLE KEYS */;

INSERT INTO `purchasing` (`id`, `kode_po`, `qty`, `tanggal_po`, `jatuh_tempo`, `supplier`, `total`, `dp`, `sisa`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'POKM18072022-1001',40,'2022-07-19','2022-07-20','Kimsin Beauty',1257000,90000,1167000,'2022-07-18 22:44:26','2022-07-18 22:53:02',NULL);

/*!40000 ALTER TABLE `purchasing` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table retur
# ------------------------------------------------------------

DROP TABLE IF EXISTS `retur`;

CREATE TABLE `retur` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_retur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_retur` date NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `retur` WRITE;
/*!40000 ALTER TABLE `retur` DISABLE KEYS */;

INSERT INTO `retur` (`id`, `no_retur`, `tanggal_retur`, `store_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'RTO18072022-1001','2022-07-19',2,'2022-07-18 23:12:36','2022-07-18 23:12:36',NULL),
	(2,'RTO18072022-1002','2022-07-19',2,'2022-07-18 23:19:38','2022-07-18 23:19:38',NULL),
	(3,'RTO18072022-1003','2022-07-19',1,'2022-07-18 23:20:56','2022-07-18 23:20:56',NULL);

/*!40000 ALTER TABLE `retur` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table retur_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `retur_items`;

CREATE TABLE `retur_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `retur_id` int(11) NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_retur` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `retur_items` WRITE;
/*!40000 ALTER TABLE `retur_items` DISABLE KEYS */;

INSERT INTO `retur_items` (`id`, `retur_id`, `product_id`, `qty_retur`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'1',1,'2022-07-18 23:12:36','2022-07-18 23:12:36',NULL),
	(2,1,'2',2,'2022-07-18 23:12:36','2022-07-18 23:12:36',NULL),
	(3,2,'1',1,'2022-07-18 23:19:38','2022-07-18 23:19:38',NULL),
	(4,2,'2',2,'2022-07-18 23:19:38','2022-07-18 23:19:38',NULL),
	(5,3,'1',1,'2022-07-18 23:20:56','2022-07-18 23:20:56',NULL),
	(6,3,'2',2,'2022-07-18 23:20:56','2022-07-18 23:20:56',NULL);

/*!40000 ALTER TABLE `retur_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sale_store
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sale_store`;

CREATE TABLE `sale_store` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode_sale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_invoice` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_sale` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table sale_store_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sale_store_items`;

CREATE TABLE `sale_store_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sale_store_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty_sale` int(11) NOT NULL,
  `harga_distributor` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table store
# ------------------------------------------------------------

DROP TABLE IF EXISTS `store`;

CREATE TABLE `store` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode_store` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;

INSERT INTO `store` (`id`, `kode_store`, `name`, `address`, `phone`, `email`, `logo`, `created_at`, `updated_at`)
VALUES
	(1,'TKM18072022-1001','relita skincare','Yogyakarta, Indonesia','085729334111','relita@gmail.com','logo.png','2022-07-18 22:42:58','2022-07-18 22:42:58'),
	(2,'TKM18072022-1002','janna skincare','Yogyakarta, Indonesia','085729312345','janna@gmail.com','logo.png','2022-07-18 22:43:35','2022-07-18 22:43:35');

/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_confirmed_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `two_factor_confirmed_at`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'kimsin','kimsin@gmail.com',NULL,'$2y$10$iZgBS0e/xXWRUlSHNJuRgORY9lvB1ATQABjLqKpkVZwrNPulLE0hC',NULL,NULL,NULL,NULL,'2022-07-18 22:40:03','2022-07-18 22:40:03');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
