# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.18-MariaDB)
# Database: studio
# Generation Time: 2022-08-24 15:12:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table announcements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `announcements`;

CREATE TABLE `announcements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;

INSERT INTO `announcements` (`id`, `title`, `description`, `status`, `link`, `created_at`, `updated_at`)
VALUES
	(1,'title','<p>deskripsi</p>','PUBLISH','link','2021-09-04 04:29:00','2021-09-04 04:29:00');

/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table banners
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table group_members
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_members`;

CREATE TABLE `group_members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_idcard` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `group_members` WRITE;
/*!40000 ALTER TABLE `group_members` DISABLE KEYS */;

INSERT INTO `group_members` (`id`, `group_id`, `member_name`, `member_idcard`, `created_at`, `updated_at`)
VALUES
	(1,1,'angora 1','4ef5acfd-fcf7-4b72-96ba-7bb561c9cae1.pdf','2021-08-12 15:31:43','2021-08-12 15:31:43'),
	(2,1,'angora 2','e4c867ce-991f-4dbc-af29-f1d2f996f0db.pdf','2021-08-12 15:31:43','2021-08-12 15:31:43'),
	(3,1,'angora 3','2066aa6c-55e3-4242-9f80-80839e9e1129.pdf','2021-08-12 15:31:43','2021-08-12 15:31:43'),
	(7,29,'Shoshana Rivas','f73af9ea-d900-429f-8201-610c47437f16.pdf','2022-07-02 05:53:01','2022-07-02 05:53:01'),
	(8,29,'Eugenia Washington','c3a41047-69c7-4c3a-85ae-2ca917ff1109.pdf','2022-07-02 05:53:01','2022-07-02 05:53:01'),
	(9,29,'Liberty Hunter','f274c5f1-a5bd-4da9-8476-fcb16eb67b04.pdf','2022-07-02 05:53:01','2022-07-02 05:53:01'),
	(10,30,'Bethany Mcdonald','6e0cbb84-7015-4563-b96a-8d19522f93b1.pdf','2022-07-02 06:10:58','2022-07-02 06:10:58'),
	(11,30,'Eaton Massey','2d5b3e58-ff76-4fa5-a168-636bebe73249.pdf','2022-07-02 06:10:58','2022-07-02 06:10:58'),
	(12,30,'Medge Langley','7b0cef0c-7ef2-417d-b0f9-da84446e87fd.pdf','2022-07-02 06:10:58','2022-07-02 06:10:58'),
	(13,31,'Thaddeus Cote','e814d85a-0414-4c8c-936e-ee998bc3acba.pdf','2022-07-02 06:24:18','2022-07-02 06:24:18'),
	(14,31,'Joy Rocha','e04aabd5-a3d2-4c92-9114-ba41b0b95527.pdf','2022-07-02 06:24:18','2022-07-02 06:24:18'),
	(15,31,'Ignatius Barr','fd155942-1d36-4d93-9c8e-8c0fd6a96ad7.pdf','2022-07-02 06:24:18','2022-07-02 06:24:18'),
	(16,32,'Alyssa Contreras','8066744b-c7f2-4404-827f-15d7a727afc1.pdf','2022-07-02 09:29:11','2022-07-02 09:29:11'),
	(17,32,'Ali Bernard','e95de1e0-781d-472e-8532-ca7dd29f6017.pdf','2022-07-02 09:29:11','2022-07-02 09:29:11'),
	(18,32,'MacKenzie Sheppard','873e6c41-b394-4a08-90e2-8c9fc480b683.pdf','2022-07-02 09:29:11','2022-07-02 09:29:11'),
	(19,33,'Mariko Blevins','b7d74c81-399f-40d6-bbcf-d6bf68c844a1.pdf','2022-07-02 10:55:20','2022-07-02 10:55:20'),
	(20,33,'Nelle Jordan','97acefe0-b10e-42f3-bb46-d9bd0aa47f81.pdf','2022-07-02 10:55:20','2022-07-02 10:55:20'),
	(21,33,'Xavier Underwood','b48726ee-b492-407a-97b6-982e763a2046.pdf','2022-07-02 10:55:20','2022-07-02 10:55:20'),
	(28,36,'Kay Stuart','28df528e-018a-4489-8457-e5d405816d73.pdf','2022-07-05 18:38:27','2022-07-05 18:38:27'),
	(29,36,'Caryn Alvarado','63c39460-c308-4fbe-9164-5b1184d43ab3.pdf','2022-07-05 18:38:27','2022-07-05 18:38:27'),
	(30,36,'Phoebe Day','d06da70b-bc05-488e-97ec-0c03b6a03d2a.pdf','2022-07-05 18:38:27','2022-07-05 18:38:27'),
	(31,37,'Malcolm Allen','3e32fdee-129e-4790-8dcf-89c60c15f6b6.pdf','2022-07-15 09:55:01','2022-07-15 09:55:01'),
	(32,37,'Tiger Avila','1eb03962-9b33-401a-bb75-a0ae2a5d9d6c.pdf','2022-07-15 09:55:01','2022-07-15 09:55:01'),
	(33,37,'Larissa Prince','8cb75fce-33f9-42d4-878b-6af24f998529.pdf','2022-07-15 09:55:01','2022-07-15 09:55:01'),
	(34,38,'Clinton Fox','811d436e-29ee-4c93-af7e-f4b243e8703b.pdf','2022-08-15 14:46:26','2022-08-15 14:46:26'),
	(35,38,'Colby Graham','70e02228-8acc-4287-a19a-cea1d6f2c97d.pdf','2022-08-15 14:46:26','2022-08-15 14:46:26'),
	(36,38,'Stewart Landry','8b0d4d9f-7f9d-44f9-8588-1247ce2cb39f.pdf','2022-08-15 14:46:26','2022-08-15 14:46:26');

/*!40000 ALTER TABLE `group_members` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil_group` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `leader_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leader_idcard` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leader_number_idcard` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statement_letter_work` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statement_letter_contest` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statement_letter_permission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `user_id`, `group_name`, `profil_group`, `leader_name`, `leader_idcard`, `leader_number_idcard`, `statement_letter_work`, `statement_letter_contest`, `statement_letter_permission`, `created_at`, `updated_at`)
VALUES
	(1,71,'','AKKF','ketua','548a45b5-7e01-4003-9ed5-4e5bc179558b.pdf','1111','0c527e20-6e33-4257-8e9a-5e6475ad1145.pdf','28571fec-2a70-41d5-95d2-7afa88aee559.pdf','8b980fcb-a597-4444-a52c-5e59ab7a6bcf.pdf','2021-08-12 15:31:43','2021-08-12 15:31:43'),
	(19,144,'','Nama Grup : MIDUN TERKENAL\r\n\r\nkomposer : Hamidun Syaputra S.Sn\r\n\r\nsinopsis karya : \r\nSINOPSIS\r\n\r\nDalam melaksanakan sebuah kegiatan, yang di lakukan secara bersama dengan tujuan mempercepat suatu perkerjaan, itulah yang di sebut dengan gotong royong. Gotong royong sendiri memiliki nilai-nilai yang positif di antaara nya, seperti membangun rasa kebersamaan, keharmonisan, menjalin solidaritas, dan mempercepat suatu perkerjaan secara bersama-sama. Konsep dari gotong royong lah yang menjadi ide dan landasan dalam penggarapan sebuah karya musik yang berjudul “Pekla Ba Goro”, yang berarti mari bergotong royong. Dalam hal ini lah pengkarya mencoba untuk menginterpretasikan nilai-nilai yang terdapat dari gotong royong tersebut kedalam sebuah karya musik dalam sajian world music.\r\n\r\n\r\nPengalaman dan Prestasi Berkesenian\r\nMidun Terkenal\r\n\r\na.	Pemusik pada event Festival Seni Budaya Se-Sumatra di Medan tahun 2017.\r\nb.	Pemusik pada event China Theatre Week di Goangzhou, China tahun 2018.\r\nc.	Pemusik pada event Pekan Komponis Indonesia di Jakarta tahun 2018 bersama komponis terpilih Avant Garde Dewa Gugat\r\nd.	 Pemusik pada event Young Composser di ISBI Bandung Bersama Diafora Musik.\r\ne.	Pemusik pada event Temu Musik di Pakan Baru pada tahun bersama Diafora 2019.\r\nf.	Komponis Terpilih pada event Pekan Komponis Muda Indonesia di Jakarta tahun \r\n2019. \r\ng.	Colabora si Butoh Indonesia, Japan, Thailand, Malaysia di ISI Padangpanjang tahun 2019.\r\nh.	Pemusik pada event Bagurau Tahun Baru Samalam Suntuak di Malaysia tahun 2019.\r\ni.	Pemusik pada event Butoh Camp, kolaborasi (Indonesia, Japang, Malaysia, Singapura, Thailand, Japan, Prancis) di Chiang Mai, Thailand tahun 2020.\r\n\r\n\r\nPendukung karya : \r\n1. Hamidun Syaputra S. Sn    : Rabab\r\n2. Rofi Rahmatullah         : Keecapi Payokumbuah\r\n3. Hedrianto                     : Kecapi Sunda\r\n4. Agung Bagaskara       : Gitar\r\n5. Sinta Ovela                  : Vokal dan Triangel\r\n6. Sri Aprillia Nura          : Vokal dan Ganto\r\n7. Rezi Satria                  : Acordion\r\n8. M. Rizki                      : Kedang Sunda\r\n9. Muhammad Widodo : Bass\r\n10. Nur Alif Ramansyah S.Sn : Talempong','Hamidun Syaputra','1a7f44c2-df8a-4bed-a2d0-84cbdfc3e0c0.pdf','1311061412960001','46ca5523-348a-498e-be33-3ae9ab8c0333.pdf','d423fb39-2223-43dd-ae35-17a74c09c7fd.pdf','b1205fbe-999a-4365-99b5-99856ddafa61.pdf','2021-07-25 15:37:22','2021-07-25 15:37:22'),
	(27,157,'','KENDHO KENCENG, merupakan kelompok musik asal Grobogan yang diketuai oleh Asep Susanto, dan beranggotakan anak-anak muda pecinta musik khususnya gamelan.','Asep Susanto','b65489eb-7f55-49ee-938f-d49fa375aa8a.pdf','3315131109940002','2b200a42-dd9c-4627-8491-a72e412ba33e.pdf','a7aa033e-b48f-41a7-a09f-74810ca76ed8.pdf','8cb214d8-7ddf-4dee-a4eb-98f18343cd31.pdf','2021-08-02 09:11:53','2021-08-02 09:11:53'),
	(36,158,'Kay Baldwin','Aliquid nulla dolore','Erasmus Carr','ecf35191-3146-4d9b-b365-1c7c6fa993d5.pdf','216','36b7f31e-3d00-407b-836e-13882f68ef80.pdf','82a9896b-a86f-4003-85a0-623aba819816.pdf','8b5b9d82-1762-4c52-9bb3-e444c3c4d4f8.pdf','2022-07-05 18:38:27','2022-07-05 18:38:27'),
	(37,160,'Dexter Neal','Facilis sit et qui c','Tashya Freeman','fdae9a70-15cc-4d53-b0a6-f1564c57d5f7.pdf','858','fd908905-1a40-4db1-b494-1123959a116e.pdf','eae62129-c694-4514-a3b4-394c35137e5c.pdf','6e721d8c-e742-406b-b811-d4312d79a893.pdf','2022-07-15 09:55:01','2022-07-15 09:55:01'),
	(38,166,'Adena Roth','Consequatur reprehe','Victor Pearson','6e3f7a42-9253-4f75-ab2e-1db25da92349.pdf','140','6ab04b58-aa09-4347-8bcd-d57ee31fecc4.pdf','f943d2e4-1819-4fc5-a26e-65d0c3c9c73d.pdf','dfd6a287-b7b1-465e-ab81-b9376bf27892.pdf','2022-08-15 14:46:26','2022-08-15 14:46:26');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2019_08_19_000000_create_failed_jobs_table',1),
	(4,'2021_05_31_153819_create_video_galleries_table',1),
	(5,'2021_06_09_051735_create_settings_table',1),
	(6,'2021_06_10_033319_create_articles_table',1),
	(7,'2021_06_10_033639_create_banners_table',1),
	(8,'2021_06_15_034156_create_announcements_table',1),
	(9,'2021_06_20_083045_add_column_user_to_video',1),
	(10,'2021_07_02_175919_create_tabel_group',1),
	(11,'2021_07_03_141005_add_songwriter_to_video_galleries',1),
	(12,'2021_07_04_091822_create_group_members_table',1),
	(13,'2021_07_10_050759_add_column_to_video_galleries',1),
	(15,'2021_08_09_073201_create_video_scores_table',2),
	(16,'2021_08_29_235104_add_column_video_galleries_status',3),
	(17,'2021_08_30_071908_add_column_to_videoscore',4),
	(20,'2021_08_30_080929_add_to_colum_videogalleries_user_jury',5),
	(21,'2021_09_01_022935_add_column_evaluate_to_video_galleries',6),
	(22,'2021_09_04_042545_add_link_to_announcements',7),
	(23,'2021_09_18_031721_add_column_video_galleries',8),
	(24,'2021_09_18_155241_add_column_video_scores',9),
	(25,'2022_07_02_053030_add_column_groups',10),
	(26,'2022_08_08_191945_add_column_vote_video_galleries',11),
	(27,'2022_08_08_192806_create_video_votes',11);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `terms_and_conditions` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `logo`, `terms_and_conditions`, `created_at`, `updated_at`)
VALUES
	(1,'default-logo.jpg','Syarat\n\n            Lomba dapat diikuti oleh mahasiswa dan umum\n            Peserta (individu atau kelompok) berdomisili di wilayah Yogyakarta dan sekitarnya\n            Merupakan karya asli (original) dan tidak mengandung unsur SARA, kekerasan dan pornografi','2021-07-10 05:10:38','2021-07-10 05:10:38'),
	(2,'default-logo.jpg','Syarat\n\n            Lomba dapat diikuti oleh mahasiswa dan umum\n            Peserta (individu atau kelompok) berdomisili di wilayah Yogyakarta dan sekitarnya\n            Merupakan karya asli (original) dan tidak mengandung unsur SARA, kekerasan dan pornografi','2021-07-19 08:32:09','2021-07-19 08:32:09');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `photo`, `name`, `email`, `role`, `status`, `phone`, `address`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'default.png','Super Admin','superadmin@gmail.com','SUPER ADMIN','PENDING','082220093199','Banyuwangi',NULL,'$2y$10$N6p4EQAdXCEfneJkG3Ce1u7SbfdzjusL88wX10lvZdPHn0FfPPkbS',NULL,'2021-07-10 05:10:38','2021-07-10 05:10:38'),
	(2,'default.png','Admin','admin@gmail.com','ADMIN','PENDING','082220093198','Banyuwangi',NULL,'$2y$10$f.GbWL3xHlsdKZ6Jsi1J.eJ95gpfZi9azWbqJoAJO9h5SK4LpYWe6',NULL,'2021-07-10 05:10:38','2021-07-10 05:10:38'),
	(4,'default.png','Default Jury','defaultjury@gmail.com','JURY','APPROVED','082220093197','Yogyakarta',NULL,'$2y$10$ye1aJcYFKz97GtONUj83K.G.gUPzerGlVrvdJPItGg.rjlpIdxQtK',NULL,'2021-07-10 05:10:38','2021-08-30 18:06:34'),
	(7,'default.png','Default User','defaultuser@gmail.com','ADMIN','VERIFY','082220093197','Banyuwangi',NULL,'$2y$10$bIeWt2.AqckMYBWApPr2xuVn8ffszjTG/R2GWFTcgQ0SGuNYn1xgq',NULL,'2021-07-19 08:32:09','2021-07-19 08:32:09'),
	(71,'default.png','1 Day Project','odaypapanyaican@gmail.com','CANDIDATE','APPROVED','085223057272','Perum Griya Banjar Raharja Blok A4 No. 19 Lingk. Haurmukti Kel/Kec. Purwaharja Kota Banjar Jawa Barat',NULL,'$2y$10$dFFrnjcQFijJNhnyLzmAzuAoplA3ORH0kWkoCSmTBZZwhLrgXbBv.',NULL,'2021-08-17 15:50:41','2021-08-17 15:50:41'),
	(72,'http://localhost/storage/settings/de691659-8a5d-47a5-b5f9-48ea7d54c870.png','juriya','juri@gmail.com','JURY','APPROVED','990900','alamat juri',NULL,'$2y$10$Y85H6hajPkXOCYS12oUp8uJpCPSJwD7wIlWh8UNNnHVUWfK5iwpsy',NULL,'2021-08-30 18:06:27','2021-08-30 18:06:27'),
	(73,NULL,'juri3','juri3@gmail.com','JURY','APPROVED','84490000','juri3 alamat',NULL,'$2y$10$cAEkYcQw.8yt/iwRyublnOSCscMhwRxg2SuwNak53J5zm9poKGmta',NULL,'2021-09-01 00:56:35','2021-09-01 00:56:35'),
	(74,'http://localhost/storage/settings/19b95e84-c680-4daa-af13-a06d6cdbe7c3.png','juri4','juri4@gmail.com','JURY','PENDING','84949000','juri4 alamat',NULL,'$2y$10$VOrr6ADs5.QJ.k6o8VbG0u.SYzgXv4T05aVbt9jbmhAqhHzq../hG',NULL,'2021-09-01 00:57:07','2021-09-01 00:57:07'),
	(75,'http://localhost/storage/settings/ae8f0748-e5f0-4667-9588-c9bf72c51071.png','manajemen','manajemen@linmtara.com','MANAJEMEN',NULL,'9990000','alamat manajemen',NULL,'$2y$10$FBBMyRhhLC/XPBrmgNpZEe214oEtenEjBBuGEFuGOFVIQcsUCsrHC',NULL,'2021-09-02 04:58:45','2021-09-02 04:58:45'),
	(76,'http://localhost/storage/settings/1573b34c-dbc4-47f7-9fd5-0c887468ec1e.png','manajemen','manajemen1@linmtara.com','CANDIDATE','PENDING','48484848','manajemen alamat',NULL,'$2y$10$JOvj/OUUFK/L3qcniClcqu.B/viaQSJR.T9ettTGPeC8EQdEk/dee',NULL,'2021-09-02 05:10:39','2021-09-16 16:17:46'),
	(77,'http://localhost/storage/settings/347f1100-9bfd-4458-9340-737fad1388b3.png','Fredy Y','fredy@gmail.com','CANDIDATE','PENDING','5555555','Yogyakarta',NULL,'$2y$10$i0mtPZFA2fYndsBih.nuauu7hZ4dSQRONzjuKR4GLsM8Zn4b880vS',NULL,'2021-09-02 05:12:32','2021-09-16 16:17:46'),
	(78,'http://localhost/storage/settings/bc0b2874-91bd-4fa2-8b65-1150e1ad3ffb.png','aku manajemen','manajemen@gmail.com','MANAJEMEN','PENDING','8889999999','alamat manajemen',NULL,'$2y$10$LOls/yIyzdBu5TRxMrbWaeqKnLc31UC9dx8C1mKlgK0pwZym20zzu',NULL,'2021-09-02 05:40:39','2021-09-02 05:40:39'),
	(140,'https://submit.linmtara.com/storage/settings/52da4040-6195-458e-b665-009a8cbb6ec5.jpg','mendadak jaming','dandessurya@gmail.com','CANDIDATE','APPROVED','081328296622','yogyakarta',NULL,'$2y$10$O3Do0qcS4uB5l6B6tGcLpOWPOcYpws1gPvjmBlnoH4BdulVzOHxiu','TibR8DtZq34oEI7bAS4KiFxOiuIsiwFYCd90CDB7tcguQYSYjTWHJc8C2Ymr','2021-07-23 00:07:59','2021-07-30 11:12:06'),
	(144,'https://submit.linmtara.com/storage/settings/358db162-48b1-449e-a32f-7ff76d4ca8e3.jpg','Hamidun syaputra s.sn , nama grup : TERKENAL ENTERTEIN , nama padepokan : padang panjang','ovelasinta@gmail.com','CANDIDATE','APPROVED','085363853501','kota Padang panjang, padang panjang barat ,provinsi sumatra barat. Jln bahder johan',NULL,'$2y$10$I9O/l7b9ASSP3jxZN7STKegOOI6dT7.WVl4pBeIg/rhqOj6Vr.aRm','wZ0NbLd6tISa3451EIWWrYUX5PfRJj1RMgB6oOYi1plEsxXz4PjliwZeeO6x','2021-07-24 09:00:47','2021-07-26 00:14:16'),
	(157,'https://submit.linmtara.com/storage/settings/f0c3fd19-5008-41d8-8eba-9fe6e2feadd8.jpg','Kendho Kenceng','asepbadrun2015@gmail.com','CANDIDATE','APPROVED','082135416123','Ds. Candisari RT 02 RW 06, Kec. Purwodadi, Kab. Grobogan',NULL,'$2y$10$sMJE8IFE0pjDw1IkvrXspua9pcu066yyn8pvlv1Muu7z7Zq.bGg6W','bDCBAKijj9DAvOOINjia8C3uUE3OXVAPUo9ua2sbEhXAIaNZZ6hZ0StDutH3','2021-08-01 13:21:53','2021-08-03 10:04:36'),
	(158,'20220714-mplswibraga.jpg','coba user 1','user@gmail.com','CANDIDATE','VERIFY','93939948488','jogjaoke ta',NULL,'$2y$10$YVwVOWt9.Mi2qpgMNf7AsOsSmI2SRrRnhzp1.uwbwGTHCZnCQrQW.',NULL,'2022-06-29 14:43:37','2022-07-14 23:20:23'),
	(159,'http://localhost/storage/settings/cdd40bc0-c016-483d-a31a-d8f67595a736.png','Ross Bush','moret@mailinator.com','CANDIDATE','PENDING','27','Nostrud culpa aperia',NULL,'$2y$10$c8pR6qOqS/g.WC8p1ax/yuF/cEo8TVzvWPU9C/MSScUYQCZQkFqYq',NULL,'2022-07-05 21:01:19','2022-07-05 21:01:19'),
	(160,'20220715-mplswibraga.jpg','borobudur oke','boro@gmail.com','CANDIDATE','APPROVED','990338389','jogja ya',NULL,'$2y$10$mSLrron6E8Xa929gspHiiuY3fatItjGQRX23NQm3zBiwQVm9LDz1q',NULL,'2022-07-15 08:41:16','2022-07-15 09:11:02'),
	(165,'https://lh3.googleusercontent.com/a/AItbvmmfXgrZLiMtKoD9lG8vWRGZ-IzUKUGTqqufZkNm=s96-c','Super Sub','prasetiyann@gmail.com','CANDIDATE','PENDING',NULL,NULL,'2022-08-10 06:41:25','0','rWI9yXhX0F0uYI0IT7W5Cdc4W7OSIffCPIZ9nK1Qe41FID2nL1mZbV7vFZfW','2022-08-10 06:41:25','2022-08-10 06:41:25'),
	(166,'http://localhost/storage/settings/3f006e8a-663a-4e29-a549-0d33f8188217.png','Gary Bowen','sypibilesa@mailinator.com','CANDIDATE','VERIFY','33','Id ducimus velit e',NULL,'$2y$10$fM.fHspB4YF8FsTwtYDX5.FrsPup.i6Ps5jteh9bn1fOqEojHcmDa',NULL,'2022-08-15 14:44:53','2022-08-15 14:44:53'),
	(167,'http://localhost/storage/settings/0ba2ba8f-bc0d-496e-813a-37a343510238.jpg','Amela Whitehead','mihid@gmail.com','CANDIDATE','APPROVED','56','Sequi atque ut aliqu',NULL,'$2y$10$1iAxonuQGSdFYuuM1rV6cuTvHR3LBtleuz4KUrak.C.h4VNK3xpRq',NULL,'2022-08-22 22:09:43','2022-08-22 22:09:43');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table video_galleries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `video_galleries`;

CREATE TABLE `video_galleries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lyrics` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `songwriter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `user_jury` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `evaluate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `final` tinyint(1) NOT NULL DEFAULT 0,
  `vote_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `video_galleries` WRITE;
/*!40000 ALTER TABLE `video_galleries` DISABLE KEYS */;

INSERT INTO `video_galleries` (`id`, `title`, `description`, `lyrics`, `songwriter`, `url`, `url_youtube`, `user_id`, `user_jury`, `status`, `evaluate`, `final`, `vote_count`, `created_at`, `updated_at`)
VALUES
	(1,'Mona Lisa','<p>sdfsdf</p>','','','http://localhost/storage/settings/19f1c14e-49b7-4b94-97eb-fbddc5e9ccae.mp4',NULL,11,NULL,0,NULL,0,0,'2021-06-20 15:23:02','2021-06-20 15:23:02'),
	(2,'Contoh judul mantba','<p>sfsf</p>','','','http://submit.linmtara.com//storage/settings/5c599b23-d7e3-4afd-a7e7-1f2337f69b14.mp4','Contoh judul oke',12,NULL,0,NULL,0,0,'2021-06-20 15:33:58','2021-06-27 17:53:46'),
	(14,'skfskf','<p>slgkasjdf</p>','','','http://submit.linmtara.com//storage/settings/0dce85ba-efc5-4678-9dd3-4508ea474ac8.mp4','sdfs',13,NULL,0,NULL,0,0,'2021-06-27 18:19:04','2021-06-27 18:19:04'),
	(18,'Mandala Sena','Mandala Sena adalah Tari Kreasi Nusantara, yang berarti menggabungkan dan mengembangkan beberapa tarian dan musik dari nusantara seperti Jawa, Sunda dan Bali. Mandala dalam bahasa Sansekerta berarti lingkaran. Mandala adalah konsep kerajaan di Indonesia pada zaman dahulu. Sedangkan Sena artinya Tentara. Musik yang epik, kolosal, dan intens menggambarkan semangat tentara untuk menyatukan nusantara. Sedangkan tariannya menggambarkan keindahan nusantara itu sendiri.','Tidak ada','Rayhan Armand Nasution','eb553dc2-4475-4db5-a559-d9a9b8d49cde.mp4',NULL,89,0,1,'LOLOS',1,0,'2021-07-12 10:02:50','2021-09-17 02:29:42'),
	(19,'1 Day Project - Indonesia Juara','Indonesia Juara merupakan lagu penyemangat, bersikap optimis, selalu bersyukur atas nikmat yang diberikan oleh Tuhan. Lirik ciptaan Yadi 1 Day Project beserta komposer lagu berkolaborasi dengan Ensamble tehnique pimpinan Indra Hermansyah mengusung musik kolaborasi Modern Etnik.','Indonesia Juara\r\nPagi yang sejuk\r\nSuasana Ceria\r\nBurung-burung berkicau merdu\r\nDalam suasana \r\nDipagi ini\r\nAkan ku nikmati semuanya\r\nBridge : \r\nGenggam tanganku, ikuti langkahku.\r\nReff :\r\nBersama kita bahagia\r\nJalankan asa dan cita-cita\r\nBersama kita ceria\r\nWujudkan cerita Indonesia Juara','Yadi 1 Day Project, kolaborasi dengan Ensamble Tehnique','61f02229-fc99-4158-b16d-332c602cd60d.mp4','https://youtu.be/vGi5Az5V3ik-11-bbbbb',71,NULL,1,'GAGAL',0,0,'2021-07-17 12:00:47','2021-09-01 03:24:18'),
	(20,'Laras','musikesj menjadi cinta alam bahagian','bahagia selamanya\r\nsejahtera bahagia\r\ncinta alam indonesia\r\nhahahahahahah','Asaal','688c5f14-bc05-4118-aef0-97cbd817d0fe.mp4',NULL,68,4,1,NULL,0,0,'2021-07-23 18:47:20','2021-08-30 08:22:06'),
	(21,'Laras','menunggu makan','es cream enak','Asaal','4a785a65-fabd-4e28-8ca3-019000cff448.mp4',NULL,143,NULL,0,NULL,0,0,'2021-07-25 21:33:48','2021-07-25 21:33:48'),
	(22,'PEKLA BA GORO','Nama Grup : MIDUN TERKENAL\r\n\r\nsinopsis karya : \r\n\r\n                                              SINOPSIS\r\n\r\nDalam melaksanakan sebuah kegiatan, yang di lakukan secara bersama dengan tujuan mempercepat suatu perkerjaan, itulah yang di sebut dengan gotong royong. Gotong royong sendiri memiliki nilai-nilai yang positif di antaara nya, seperti membangun rasa kebersamaan, keharmonisan, menjalin solidaritas, dan mempercepat suatu perkerjaan secara bersama-sama. Konsep dari gotong royong lah yang menjadi ide dan landasan dalam penggarapan sebuah karya musik yang berjudul “Pekla Ba Goro”, yang berarti mari bergotong royong. Dalam hal ini lah pengkarya mencoba untuk menginterpretasikan nilai-nilai yang terdapat dari gotong royong tersebut kedalam sebuah karya musik dalam sajian world music.','hei ya hey yahey\r\nhei ya hey yahey\r\nhei ya hee aaa aaa aaa eyyy 2x\r\n\r\nintro....\r\n\r\nhaa haaa haa haa haa haa\r\n\r\nHei uda uni teman teman semuanya\r\nMarilah kita bersama bergotong royong\r\nMembangun kampung tentunya yang kita cinta\r\n\r\nHei uda uni teman teman semuanya\r\nMarilah kita bersama bergotong royong\r\nMembangun kampung tentunya yang kita cinta\r\n\r\nHei uda uni teman teman semuanya\r\nMarilah kita bersama bergotong royong\r\nMembangun kampung tentunya yang kita cinta.... heptati\r\n\r\nHei uda uni teman teman semuanya\r\nMarilah kita bersama bergotong royong\r\nMembangun kampung tentunya yang kita cinta\r\n\r\nMasa lalu telah berlalu\r\nMasa kini masa depan\r\nWahai kwan  temn semua\r\nDimnaapun kalian berada\r\nWalaupun pandemi ada\r\nGotong royong janganlah lupa 2x\r\n\r\nhooooooo......','komposer : Hamidun Syaputra S.Sn','f9af3966-881b-4a0e-9b56-e1e99f358e65.mp4','https://youtu.be/y5NJOFfNy2Q',144,0,1,'LOLOS',1,0,'2021-07-26 09:07:23','2021-09-18 03:26:39'),
	(23,'Tanah Air Indonesia','Karya ini sebagai pengalaman empiris komposer dalam melihat indonesia. Banyak hal yang dirasakan saat melihat Negri ini yang penuh dengan keragaman. Ide Musikal yang tumbuh dalam karya ini terkumpul dari setiap memori yang didapatkan komposer dari mengenal indonesia secara umum, maupun secara mendalam.','Tanah Air Indonesia\r\n\r\nKilau cahaya Langit Nusantara\r\nMewarnai Katulistiwa......\r\nSang saka dwi warna\r\nBerkibar di cakrawala....\r\n                 Puja puji alam nan mulya\r\n                 Kidung cinta dari sang maha\r\n                 setiap suara mewarnai tanah ku \r\n                 tanah surga\r\nReff:\r\nTanah air Indonesia \r\nNegri Tercinta\r\njanji kami bakti mengabdi\r\nIndonesia.....\r\nNegri tercinta \r\n\r\n                 Puja puji alam nan mulya\r\n                 Kidung cinta dari sang maha\r\n                 setiap suara mewarnai tanah ku \r\n Puja puji alam nan mulya\r\n  Kidung cinta dari sang maha\r\n  setiap suara mewarnai tanah ku \r\n   tanah surga\r\n\r\nBack to Reff:','Surya Dandes','b2dfa250-b21e-46d4-b723-1db345b9705c.mp4',NULL,140,72,1,'LOLOS',1,0,'2021-08-01 15:23:43','2022-07-02 14:20:19'),
	(24,'Alam Terindah','Alam nusantara tiada duanya','ku tatap langit merah bening segar\r\nku basuh wajah yang kusam\r\npagi mulai hidup di cakrawala\r\nmerah mewarnai indah bagai lukisan\r\nsang surya menampakkan terangnya\r\nmembuka mata menjelang cahayanya','tutu dan keple','a7ab2485-55c1-409a-9ead-047a542ad7e4.mp4',NULL,106,NULL,0,NULL,0,0,'2021-08-03 14:00:16','2021-08-03 14:00:16'),
	(25,'Sri mulyani','deskripsi karya si mulyani','lirik ya','sri mulyani','c65a8e66-a651-4c3e-a316-6a528dcc429a.mp4',NULL,57,NULL,0,NULL,0,0,'2021-08-04 22:09:45','2021-08-04 22:09:45'),
	(26,'HARMONI NUSANTARA','Harmoni Nusantara\r\n\r\nKarya musik yang tercipta dari sebuah interpretasi keragaman Nusantara. Indonesia kaya akan keberagaman suku, bahasa, budaya, agama atau kepercayaan, adat istiadat dengan kondisi geografis yang sangat menguntungkan. Keindahan alam dan sumber daya yang terkandung merupakan anugrah dari yang Maha Kuasa. Perbedaan menjadi suatu warna tersendiri dan terikat erat oleh persatuan seperti yang tercermin dalam Bhineka Tunggal Ika. \r\nPeradaban luhur warisan nenek moyang melahirkan kebudayaan yang berwarna. Khususnya ragam kesenian yang dimiliki, sarat akan makna filosofis, pendidikan, pesan moral, amanat dan sopan santun. Tradisi kerakyatan yang terkandung pada setiap wilayah memiliki corak yang khas, produk salah satunya adalah Seni Musik. \r\nHarta budaya yang tak ternilai harganya menjadi sebuah kebanggaan akan kreatifitas masyarakatnya. Alunan bunyi dan nada-nada merupakan sebuah gambaran kreativitas, imajinasi dari setiap penciptanya. Musik tradisi daerah yang sangat estetik, serta alat instrumennya menjadi sebuah identitas daerah. Pola, teknik, permainan alat musik daerah dengan kerumitannya, menjadi tantangan yang menarik untuk dituangkan dalam sebuah karya. \r\nPaduan bunyi, nyanyian atau permainan musik yang menggunakan dua nada atau lebih yang berbeda tinggi nadanya dan dibunyikan secara serentak akan tercipta Harmoni. Berpijak dari kata Harmoni tersebut, mencoba merepresentasikan sebuah karya musik baru dengan menghadirkan bagian-bagian kecil dari khasanah musik Nusantara. Gaya Banyuwangi, Bali, dan Surakarta, merupakan bagian kecil dari kekayaan Nusantara untuk mewakili keberagaman, dengan dibumbui eksplorasi untuk mencapai sebuah karya musik baru yang maksimal. \r\nHarmoni Nusantara adalah capaian dalam karya ini, walaupun tidak mungkin dapat membingkai seluruh keragaman musik Indonesia, namun semangat kekaryaan menjadi kunci utama proses terciptanya sebuah karya ini. Wujud kebanggaan, kecintaan terhadap karya tradisi Nusantara dengan menjunjung tinggi nilai kebudayaan. Budaya adalah cermin kepribadian bangsa, bersyukur atas anugrah Harmoni Nusantara yang menjadikan Ragam Indonesia.','HARMONI NUSANTARA\r\nKARYA: ASEP SUSANTO\r\n\r\nInilah negriku bagai surga dunia…\r\nNusantara…\r\nWalau beda tetap bersatu Indonesia\r\nSubur makmur termasyur nan berbudi luhur\r\nBerjajar pulau digaris khatulistiwa\r\nTanah indah anugrah\r\nMenghampar lautan samudra terbentang luas\r\nSumatra Jawa Madura Nusa Tenggara\r\nSulawesi Bali Kalimantan Papua\r\nDan beribu Pulau lainnya\r\nRagam suku, bahasa, budaya, adat dan istiadat\r\nHasil alam, makanan, dan kerajinan tangan\r\nBerjuta wisata serta flora fauna\r\nIndonesia penuh pesona….\r\n\r\n\r\n\r\nVokal Tunggal:  [ Eman, yara\r\n                   ketang-ketang ya gaduk mrene,\r\n                   janji kadung jare paman yara semayanan ]\r\n\r\nRagam seni musik dan bunyi\r\nNada – nada,  Da Mi Na Ti La,   Ding Dong Deng Dung,   Na No Ne Ni,   Pi Ma\r\nJalin menjalin bunyi\r\nTabuhan Nusantara,\r\nKetuk tilu jaipong\r\nKothekan dan Timpalan,\r\nGerongan, senggakan, sindenan, kombangan,\r\nHarmoni tradisi\r\nKhasanah Budaya\r\nRagam Indonesia…','ASEP SUSANTO','8433adbf-80a2-439c-9ee6-8646a452beb2.mp4',NULL,157,NULL,0,NULL,0,0,'2021-08-04 22:21:52','2021-08-04 22:21:52'),
	(27,'Tumaritis','Lagu yang berjudul Tumatritis diciptakan oleh Sanggar Rejo. \r\nTumaritis menceritakan tentang kehidupan manusia seharusnya hidup berdampingan dalam harmoni saling gotong royong, Bermanfaat satu sama lain, saling menghormati bahkan dapat berdampingan dengan makhluk lainya tanpa saling merugikan.','-Tumaritis-\r\nUrip Kudu\r\n(Hidup Itu)\r\nMigunani\r\n(Bermanfaat)\r\nGotong Royong\r\n(Gotong Royong)\r\nOjo Lali\r\n(Jangan Lupa)\r\nBecik olo\r\n(Baik buruk)\r\nSoko Gusti\r\n(Dari Gusti)\r\n\r\nDilakoni\r\n(Lakukan)\r\nSing Setiti\r\n(yang Cermat dan hati-hati)\r\nora perlu\r\n(tidak Perlu)\r\nPodo Ngangluh\r\n(mengeluh)\r\nTetep Nyawiji\r\n(tetap Bersatu)\r\nIbu Bumi\r\n(Ibu Bumi)\r\nSing Ngajari\r\n(yang mengajari)\r\n\r\nSejatine\r\n(sesungguhnya)\r\nUrip Iku\r\n(hidup itu)\r\nBebarengan\r\n(Bersama-sama)\r\nMukarabi\r\n(saling Bermanfaat)\r\nMapan, Mituhu\r\n(Memiliki ketahanan fisik dan mental yang tangguh)\r\nManembah, Marambah\r\n(Sadar diri, Berada di depan memberi kebaikan)\r\nterus obah\r\n(lalu Bergerak)\r\nlan Ngrumati\r\n(dan Merawat)\r\nGawe Brayan\r\n(Untuk Bersama)\r\nBagas Waras\r\n(sehat Kuat)\r\nRino Wengi\r\n(Siang Malam)\r\nGuyub Rukun\r\n(Bersama-sama Rukun)\r\nLan Ngayomi\r\n(Untuk Melindungi)\r\nBumi Aji\r\n(Tanah yang Terhormati)','Sanggar Rejo','1cc88ca1-91c2-4055-aa63-620a1cbe9d86.mp4','https://youtu.be/di0WajmjQ8U',131,NULL,0,NULL,0,0,'2021-08-05 11:47:27','2021-08-05 11:47:27'),
	(28,'CILUK BAKEKOK','Setiap orang memiliki ambisi masing-masing dalam hidupnya. Entah itu ambisi dalam berkarier, pendidikan, hingga pencapaian. Karena dengan memiliki ambisi, kita dapat semangat dalam mengejar impian dan cita-cita yang ingin kita wujudkan di masa mendatang.\r\nNamun ternyata sifat ambisius itu berubah menjadi petaka jika seseorang berubah menjadi \"terlalu ambisius\". Memiliki ambisi berlebih terhadap sesuatu ternyata dapat membuat hidup menjadi tidak bahagia hingga dapat merusak mental dan fisik kita sendiri.','“CILUK BAKEKOK”\r\n\r\nCiluk Bakekok...................			 3 X\r\nCiluk Bakekok...................			 3 X\r\n\r\n\r\nSina Kapanggih					 2 X\r\nMun kapanggih keur si ujang\r\nSina Kapanggih	\r\nMun kapanggih keur si ujang\r\n\r\nCiluk Bakekok...................			 3 X\r\n\r\nTuh nyai tinggali aya nu matana hiji\r\nMatana ngan ukur dunya, taya pisan aheratna\r\nAduh cilaka.\r\n\r\nTuh nyai tinggali aya nu matana hiji\r\nSuka siku ka sasama, hirupna teu make rasa, Sok sangeunahna\r\n\r\nCiluk Bakekok...................			 3 X\r\nCiluk Bakekok...................			 3 X\r\n\r\nTah ujang regeupkeun\r\nTong kabawa ku si eta\r\nNgabibita saedanna\r\nSarakah taya cegahna\r\nKaduhung engkena\r\n\r\nTuh nyai tinggali aya nu matana hiji\r\nMatanya ngan ukur dunya, taya pisan aheratna\r\nAduh cilaka.\r\n\r\nCiluk Bakekok...................			 3 X\r\nCiluk Bakekok...................			 3 X\r\n\r\nArtinya;\r\n“ CILUK BAKEKOK “\r\n\r\nCiluk Bakekok...................	\r\nCiluk Bakekok...................	\r\n\r\nBiar Ketemu\r\nKalau ketemu buat si bujang\r\nBiar Ketemu				\r\nKalau ketemu buat si ujang\r\n\r\n           Ciluk Bakekok...................\r\n\r\nTuh nyai lihat ada yang matanya satu\r\nMatanya cuma buat dunia, sama sekali tidak ada ahiratnya\r\nAduh celaka\r\n\r\nTuh nyai lihat ada yang matanya satu\r\n................................, hidupnya ngak pake rasa, se’enaknya saja\r\n\r\nCiluk Bakekok...................	\r\nCiluk Bakekok...................	\r\n\r\nNah bujang deungerin\r\nJangan ikut ikutan sama dia\r\nNgerayu se’edan-nya\r\nSerakah ngak ada batas-nya\r\nNyesel nantinya\r\n\r\nTuh nyai lihat ada yang matanya satu\r\nMatanya cuma buat dunia, sama sekali tidak ada ahiratnya\r\nAduh celaka','GUNAPANSIRNA.SSB','56dd6113-7246-49c8-9571-97503c92c22a.mp4','890-8bob4zs',108,NULL,1,NULL,0,0,'2021-08-08 19:39:22','2021-09-01 00:59:45'),
	(29,'ROSULULLOH NU MULYA','Sholawat adalah bentuk pujian dan cara umat Islam bersilaturrahim kepada Nabi Muhammad SAW. Selain itu, sholawat juga mempunyai makna sebagai bentuk kepedulian sosial. Hal ini menunjukkan betapa Islam sangat rahmatan lil alamin karena mengajarkan tidak egois dan senantiasa memberikan berkah bagi semua bagi semua umat. Nama yang paling sering disebut setiap hari ialah Nabi Muhammad Saw. Penyebutan nama Nabi lebih banyak dilakukan dalam bentuk sholawat. Sholawat atau selawat adalah ungkapan rasa cinta dan kerinduan kepada Nabi Muhammad dengan mengucapkan lafaz-lafaz shalawat, seperti Allahumma shalli „ala Muhammad. Bershalawat kepada Nabi merupakan seruan Allah SWT.','“ ROSULULLOH NU MULYA “\r\nRosululloh yang mulya\r\n\r\nSholawat ;\r\nMaula Ya sholi wa salim lila imanna abada, ala habibika khoiril khadqi ulihirmi.....................................\r\n\r\nMaula Ya sholi wa salim lila imanna abada, ala habibika khoiril khadqi ulihirmi\r\nYa robbibil musthofa, baligh maqo sidana waghfirlana mamagho, ya’a wasial karomi\r\nHu’wal habibu ladzi turja syafa atuhu, li ulti haoli minal ahwali muktahimi\r\n\r\nDuh rosululloh nu mulya\r\nYa rosululloh yang mulya\r\nPancarkeun sadaya rasa\r\nPencarkan semua rasa\r\nHibarkeun cahya salira\r\nKibarkan sinar anda\r\nDi abdi-abdi sadaya\r\nDi kami-kami semua\r\n\r\nContoh amal kemulyaan\r\nSungguh tiada tandingan\r\nSemoga kami sekalian\r\nIzin alloh bisa menirukan','LS.SIDDIQ PAWITRA / TUNGGAL KUSUMAH','ac733dd7-5c49-463b-a826-f42628a4dd54.mp4','QLyqJ7E0wRQ',177,NULL,0,NULL,0,0,'2021-08-09 10:08:28','2021-08-09 10:08:28'),
	(37,'Eu est quas ullamco','Eaque qui minus cupi','Quia iusto sit fuga','Aut qui commodi aspe','videocoba.mp4-20220715-borobudur oke',NULL,0,NULL,0,NULL,0,0,'2022-07-15 10:05:16','2022-07-15 10:05:16'),
	(38,'Corrupti ut officia','Id mollit non velit','Illum culpa offici','Aut tempora dolores','20220715_borobudur oke_videocoba.mp4',NULL,0,NULL,0,NULL,0,0,'2022-07-15 10:06:39','2022-07-15 10:06:39'),
	(39,'Duis saepe facilis a','Exercitationem fugia','Est animi voluptate','Officia dolore saepe','20220715_borobudur oke_videocoba.mp4',NULL,0,NULL,0,NULL,0,0,'2022-07-15 10:30:09','2022-07-15 10:30:09'),
	(40,'Et anim irure sit e','Ex laborum Iure vol','Cumque quo nisi et a','Nisi amet possimus','20220715_borobudur oke_videocoba.mp4',NULL,0,NULL,0,NULL,0,0,'2022-07-15 10:44:34','2022-07-15 10:44:34'),
	(41,'Porro ipsa ea ea ni','Duis voluptas sit di','Ea modi dolores aut','Et aliqua Dolore en','20220715_borobudur oke_videocoba.mp4',NULL,0,NULL,0,NULL,0,0,'2022-07-15 10:47:14','2022-07-15 10:47:14'),
	(42,'In sequi rerum simil','Iusto dolores molest','Est ut consequatur','Provident veniam s','20220715_borobudur oke_videocoba.mp4',NULL,0,NULL,0,NULL,0,0,'2022-07-15 11:01:32','2022-07-15 11:01:32'),
	(43,'Irure ad omnis sit m','Inventore quo molest','Sed ut repellendus','Sint quae qui recus','20220715_borobudur oke_videocoba.mp4',NULL,0,NULL,0,NULL,0,0,'2022-07-15 11:02:09','2022-07-15 11:02:09'),
	(44,'Irure ad omnis sit m','Inventore quo molest','Sed ut repellendus','Sint quae qui recus','20220715_borobudur oke_videocoba.mp4',NULL,0,NULL,0,NULL,0,0,'2022-07-15 11:02:12','2022-07-15 11:02:12'),
	(45,'Irure ad omnis sit m','Inventore quo molest','Sed ut repellendus','Sint quae qui recus','20220715_borobudur oke_videocoba.mp4',NULL,0,NULL,0,NULL,0,0,'2022-07-15 11:20:42','2022-07-15 11:20:42'),
	(46,'Officia voluptatibus','Distinctio Perspici','Rerum excepturi recu','Inventore corporis d','20220715_borobudur oke_videocoba.mp4',NULL,160,NULL,0,NULL,0,0,'2022-07-15 11:22:24','2022-07-15 11:22:24'),
	(47,'Ullamco id voluptas','Incididunt sint ill','Velit et laboriosam','Sit aliqua Deleniti','20220822_Amela Whitehead_Screen Recording 2021-06-21 at 09.35.14.mp4',NULL,160,NULL,0,NULL,0,0,'2022-08-22 22:15:45','2022-08-22 22:15:45'),
	(48,'Cum eum tenetur quae','Tenetur incididunt m','Quod corporis lorem','Ipsum minima cillum','20220822_Amela Whitehead_upload_video_coba.mp4',NULL,150,NULL,0,NULL,0,0,'2022-08-22 22:36:32','2022-08-22 22:36:32'),
	(49,'Proident consectetu','Enim ex accusantium','Optio rerum rerum m','Id amet minima exce','20220822_Amela Whitehead_Screen Recording 2021-06-21 at 09.35.14.mp4',NULL,66,NULL,0,NULL,0,0,'2022-08-22 22:50:11','2022-08-22 22:50:11'),
	(50,'Fugit tempor a nisi','Fuga Aut aliquam co','Suscipit sed dolore','Exercitation non mol','202208222314upload_video_coba.mp4',NULL,77,NULL,0,NULL,0,0,'2022-08-22 23:14:12','2022-08-22 23:14:12'),
	(51,'Cillum ad voluptates','Saepe aliquid et id','Accusantium blanditi','Veniam sed eiusmod','20220822_Amela Whitehead_videokuya.mp4',NULL,99,NULL,0,NULL,0,0,'2022-08-22 23:17:53','2022-08-22 23:17:53'),
	(52,'Aspernatur ad natus','Quam doloremque sunt','Similique dolorem ar','Et debitis quibusdam','20220823_Amela Whitehead_upload_video_coba.mp4',NULL,90,NULL,0,NULL,0,0,'2022-08-23 20:59:11','2022-08-23 20:59:11'),
	(53,'Qui exercitation atq','Earum accusantium la','Officiis fuga Magni','Et nulla sed veniam','images/video/2022/pr-amela-whitehead-20220823-212348.mp4',NULL,44,NULL,0,NULL,0,0,'2022-08-23 21:23:48','2022-08-23 21:23:48'),
	(54,'In sit et qui aperi','Ea quis dolores obca','Deserunt et doloremq','Voluptate cum quae d','videos/4sMZMQDGnUUroewPmbhvEUN2VwqHbyQGArgz1HZv.mp4',NULL,21,NULL,0,NULL,0,0,'2022-08-23 21:37:54','2022-08-23 21:37:54'),
	(55,'In quaerat fugiat pa','Rerum quidem optio','Soluta aut dignissim','Est deserunt anim do','videos/upload_video_coba.mp4',NULL,22,NULL,0,NULL,0,0,'2022-08-23 22:16:54','2022-08-23 22:16:54'),
	(56,'Autem sint voluptate','Corrupti corporis a','Fuga Eius alias hic','Reprehenderit iusto','videos/upload_video_coba.mp4',NULL,23,NULL,0,NULL,0,0,'2022-08-23 22:19:08','2022-08-23 22:19:08'),
	(57,'Voluptates quis reru','Labore in eligendi c','Non voluptates in el','Esse corporis ut in','https://res.cloudinary.com/prasven/video/upload/v1661297869/linmtara/prasven.mp4',NULL,24,NULL,0,NULL,0,0,'2022-08-24 06:37:42','2022-08-24 06:37:42'),
	(58,'Quis iure ullamco te','Aliquam quia totam m','Et id sint exercitat','Inventore sint vel','1e0ce93d-26a1-453c-ae44-7c6cdc1cdc26.mp4',NULL,25,NULL,0,NULL,0,0,'2022-08-24 08:24:46','2022-08-24 08:24:46'),
	(59,'Iusto quisquam molli','Ut ea iste consequat','Sunt maxime corpori','Error iste molestiae','https://linmtara-storage1.sgp1.digitaloceanspaces.com/video//upload_video_coba.mp4',NULL,167,NULL,0,NULL,0,0,'2022-08-24 08:34:13','2022-08-24 08:34:13');

/*!40000 ALTER TABLE `video_galleries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table video_scores
# ------------------------------------------------------------

DROP TABLE IF EXISTS `video_scores`;

CREATE TABLE `video_scores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `video_gallery_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_jury` int(11) NOT NULL,
  `score_original` int(11) NOT NULL,
  `score_mastery` int(11) NOT NULL,
  `score_harmony` int(11) NOT NULL,
  `score_dynamics` int(11) NOT NULL,
  `score_creation` int(11) NOT NULL,
  `score_presentation` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `final` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `video_scores` WRITE;
/*!40000 ALTER TABLE `video_scores` DISABLE KEYS */;

INSERT INTO `video_scores` (`id`, `video_gallery_id`, `user_id`, `user_jury`, `score_original`, `score_mastery`, `score_harmony`, `score_dynamics`, `score_creation`, `score_presentation`, `total`, `final`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(11,19,71,72,90,78,67,77,66,89,78,0,NULL,'2021-08-30 18:07:48','2021-08-30 18:07:48'),
	(12,28,108,72,56,78,99,75,45,67,70,0,NULL,'2021-08-30 18:08:57','2021-08-30 18:08:57'),
	(13,23,140,73,56,88,99,67,45,89,74,0,NULL,'2021-09-01 00:57:51','2021-09-01 00:57:51'),
	(14,19,71,73,55,34,56,76,98,46,61,0,NULL,'2021-09-01 00:58:42','2021-09-01 00:58:42'),
	(15,28,108,73,43,73,75,82,97,45,69,0,NULL,'2021-09-01 00:59:45','2021-09-01 00:59:45'),
	(17,22,144,72,33,55,66,44,90,44,55,0,NULL,'2021-09-07 16:57:19','2021-09-07 16:57:19'),
	(18,23,140,72,59,89,89,90,44,98,78,0,NULL,'2021-09-07 16:57:52','2021-09-07 16:57:52'),
	(29,18,89,72,100,100,100,100,100,100,100,1,NULL,'2021-09-18 15:57:55','2021-09-18 15:57:55'),
	(31,22,144,72,100,100,100,100,100,100,100,1,NULL,'2021-09-18 16:18:23','2021-09-18 16:18:23');

/*!40000 ALTER TABLE `video_scores` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table video_votes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `video_votes`;

CREATE TABLE `video_votes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_gallery_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `video_votes` WRITE;
/*!40000 ALTER TABLE `video_votes` DISABLE KEYS */;

INSERT INTO `video_votes` (`id`, `user_id`, `ip_address`, `video_gallery_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'127.0.0.1',46,'2022-08-10 06:35:15','2022-08-10 06:35:15'),
	(2,160,'127.0.0.1',46,'2022-08-10 06:41:42','2022-08-10 06:41:42'),
	(3,165,'127.0.0.1',46,'2022-08-10 06:42:39','2022-08-10 06:42:39');

/*!40000 ALTER TABLE `video_votes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
